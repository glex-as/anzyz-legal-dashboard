﻿using BestHTTP;
using Goontech.Anzyz_API_Client.Scripts.Models.Auth;
using Goontech.Anzyz_API_Client.Scripts.Models.Search;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Goontech.Anzyz_API_Client.Scripts
{
    public class AnzyzApiClient
    {
        public event Action OnLoginSuccess;
        public event Action<string> OnLoginError;

        private readonly string _authUrl;
        private readonly string _baseUrl;

        private string _accessToken = "";

        public AnzyzApiClient(string authUrl, string baseUrl)
        {
            _authUrl = authUrl;
            _baseUrl = baseUrl;
        }

        #region Authentication

        /// <summary>
        /// Login / Authenticate a user account
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="clientSecret"></param>
        /// <param name="audience"></param>
        /// <param name="grantType"></param>
        public void Login(string clientId, string clientSecret, string audience, string grantType)
        {
            AuthRequest authRequest = new AuthRequest(clientId, clientSecret, audience, grantType);

            string json = JsonConvert.SerializeObject(authRequest);

            HTTPRequest req = new HTTPRequest(new Uri(_authUrl + "/oauth/token"), HTTPMethods.Post, OnLoginComplete);
            req.AddHeader("content-type", "application/json");
            req.RawData = new UTF8Encoding().GetBytes(json);
            req.Send();
        }

        private void OnLoginComplete(HTTPRequest request, HTTPResponse response)
        {
            if (response.IsSuccess)
            {
                AuthResponse authResponse = JsonConvert.DeserializeObject<AuthResponse>(response.DataAsText);

                if (authResponse.access_token == null)
                {
                    OnLoginError?.Invoke("Login failed. You might need to set a new password.");

                    Debug.Log($"Anzyz login failed with code: {response.Message}.");
                }
                else
                {
                    _accessToken = authResponse.access_token;

                    OnLoginSuccess?.Invoke();
                }
            }

            else
            {
                OnLoginError?.Invoke($"Login failed with message: {response.Message} and status code: {response.StatusCode}");
            }
        }

        #endregion

        #region Search

        public void Topic(TopicRequestModel model, OnRequestFinishedDelegate callback)
        {
            string json = JsonConvert.SerializeObject(model);

            HTTPRequest req = NewPostHttpRequest("full/topic", callback);
            req.RawData = new UTF8Encoding().GetBytes(json);
            req.Send();
        }

        /// <summary>
        /// GET method to find category using category ID.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="callback"></param>
        public void GetCategoriesById(string categoryId, OnRequestFinishedDelegate callback)
        {
            HTTPRequest req = NewGetHttpRequest($"full/categories/{categoryId}", callback);
            req.Send();
        }

        /// <summary>
        /// GET method to find all categories
        /// </summary>
        /// <param name="callback"></param>
        public void GetCategories(OnRequestFinishedDelegate callback)
        {
            HTTPRequest req = NewGetHttpRequest("full/categories", callback);
            req.Send();
        }

        /// <summary>
        /// POST method to save categories
        /// </summary>
        /// <param name="description"></param>
        /// <param name="id"></param>
        /// <param name="pageSize"></param>
        /// <param name="query"></param>
        /// <param name="topicReqModel"></param>
        /// <param name="callback"></param>
        public void SaveCategories(string description, string id, int pageSize, Dictionary<string, object> query, TopicRequestModel topicReqModel, OnRequestFinishedDelegate callback)
        {
            var model = new CategoryRequestModel(description, id, pageSize, query, true, topicReqModel);
            string json = JsonConvert.SerializeObject(model);

            HTTPRequest req = NewPostHttpRequest("full/savecategories", callback);
            req.RawData = new UTF8Encoding().GetBytes(json);
            req.Send();
        }

        /// <summary>
        /// POST method to start aggregate analysis
        /// Returns key for retrieving the aggregate and a boolean indicating whether the aggregate is resolved or not.
        /// </summary>
        /// <param name="wavePrediction"></param>
        /// <param name="callback"></param>
        /// <param name="invert"></param>
        /// <param name="pageSize"></param>
        /// <param name="knowledgeBaseName"></param>
        /// <param name="targetPhrasesModel"></param>
        public void StartAggregate(bool invert, int pageSize, string knowledgeBaseName, TargetPhrasesModel targetPhrasesModel, bool wavePrediction, OnRequestFinishedDelegate callback)
        {
            var a = ""; // Todo: Replace with proper model

            var model = new AggregateRequestModel(a, invert, pageSize, knowledgeBaseName, targetPhrasesModel, wavePrediction);
            string json = JsonConvert.SerializeObject(model);

            HTTPRequest req = NewPostHttpRequest("full/startaggregate", callback);
            req.RawData = new UTF8Encoding().GetBytes(json);
            req.Send();
        }

        /// <summary>
        /// POST method to retrieve aggregate analysis
        /// Requires key from <inheritdoc cref="StartAggregate"/>
        /// </summary>
        /// <param name="wavePrediction"></param>
        /// <param name="callback"></param>
        /// <param name="id"></param>
        public void RetrieveAggregate(string id, bool wavePrediction, OnRequestFinishedDelegate callback)
        {
            var doc = new RetrieveAggregateRequestModel(id, wavePrediction);
            string json = JsonConvert.SerializeObject(doc);

            HTTPRequest req = NewPostHttpRequest("full/retrieveaggregate", callback);
            req.RawData = new UTF8Encoding().GetBytes(json);
            req.Send();
        }

        /// <summary>
        /// POST method to get csv document
        /// </summary>
        /// <param name="scoreThreshold"></param>
        /// <param name="callback"></param>
        /// <param name="ids"></param>
        /// <param name="creationTime"></param>
        /// <param name="jointExtIds"></param>
        /// <param name="sampleSize"></param>
        public void CategorizeCsv(List<string> ids, DateTime creationTime, List<string> jointExtIds, int sampleSize, int scoreThreshold, OnRequestFinishedDelegate callback)
        {
            var dict = new Dictionary<string, object>();
            dict.Add("ext_ids", ids);
            dict.Add("creation_time", creationTime);
            dict.Add("joint_ext_ids", jointExtIds);
            dict.Add("samplesize", sampleSize);
            dict.Add("scorethreshold", scoreThreshold);

            string json = JsonConvert.SerializeObject(dict);

            HTTPRequest req = NewPostHttpRequest("full/categorize/csv", callback);
            req.RawData = new UTF8Encoding().GetBytes(json);
            req.Send();
        }

        /// <summary>
        /// POST method to get categorize
        /// </summary>
        /// <param name="model"></param>
        /// <param name="callback"></param>
        public void Categorize(QueryModel model, OnRequestFinishedDelegate callback)
        {
            string json = JsonConvert.SerializeObject(model);
            HTTPRequest req = NewPostHttpRequest("full/categorize", callback);
            req.RawData = new UTF8Encoding().GetBytes(json);
            req.Send();
        }

        /// <summary>
        /// POST method to get phrase counts
        /// </summary>
        /// <param name="callback"></param>
        public void PhraseCounts(OnRequestFinishedDelegate callback)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GET method to retrieve sources
        /// </summary>
        /// <param name="callback"></param>
        public void Sources(OnRequestFinishedDelegate callback)
        {
            HTTPRequest req = NewGetHttpRequest("full/sources", callback);
            req.Send();
        }

        /// <summary>
        /// POST method to retrieve list of distinct values of chosen attribute
        /// </summary>
        /// <param name="callback"></param>
        public void Unique(OnRequestFinishedDelegate callback)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// POST method to retrieve distinct values of chosen attribute and the count of each value
        /// Can be filtered creation time and sorted by count or value
        /// </summary>
        /// <param name="callback"></param>
        public void AttributeDistribution(OnRequestFinishedDelegate callback)
        {
            throw new NotImplementedException();
        }



        #endregion

        #region Generic HTTP requests

        /// <summary>
        /// Get's content from a full url (NOT pre-formatted API url)
        /// </summary>
        /// <param name="url"></param>
        /// <param name="callback"></param>
        public void GetContentFromUrl(string url, OnRequestFinishedDelegate callback)
        {
            HTTPRequest req = new HTTPRequest(new Uri(url), HTTPMethods.Get, callback);
            req.Send();
        }

        /// <summary>
        /// GET request with Authorization headers and base uri included. Only needs a delegate and path.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="callbackDelegate"></param>
        /// <returns></returns>
        public HTTPRequest NewGetHttpRequest(string path, OnRequestFinishedDelegate callbackDelegate)
        {
            string fullUrl = _baseUrl + path;

            HTTPRequest req = new HTTPRequest(new Uri(fullUrl), HTTPMethods.Get, callbackDelegate);
            req.AddHeader("content-type", "application/json");
            req.AddHeader("authorization", "Bearer " + _accessToken);

            return req;
        }

        /// <summary>
        /// POST request with Authorization headers and base uri included. Only needs a delegate and path.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="callbackDelegate"></param>
        /// <returns></returns>
        public HTTPRequest NewPostHttpRequest(string path, OnRequestFinishedDelegate callbackDelegate)
        {
            string fullUrl = _baseUrl + path;

            HTTPRequest req = new HTTPRequest(new Uri(fullUrl), HTTPMethods.Post, callbackDelegate);
            req.AddHeader("content-type", "application/json");
            req.AddHeader("authorization", "Bearer " + _accessToken);

            return req;
        }

        /// <summary>
        /// DELETE request with Authorization headers and base uri included. Only needs a delegate and path.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="callbackDelegate"></param>
        /// <returns></returns>
        public HTTPRequest NewDeleteHttpRequest(string path, OnRequestFinishedDelegate callbackDelegate)
        {
            string fullUrl = _baseUrl + path;

            HTTPRequest req = new HTTPRequest(new Uri(fullUrl), HTTPMethods.Delete, callbackDelegate);
            req.AddHeader("content-type", "application/json");
            req.AddHeader("authorization", "Bearer " + _accessToken);

            return req;
        }

        /// <summary>
        /// UPDATE request with Authorization headers and base uri included. Only needs a delegate and path.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="callbackDelegate"></param>
        /// <returns></returns>
        public HTTPRequest NewPutHttpRequest(string path, OnRequestFinishedDelegate callbackDelegate)
        {
            string fullUrl = _baseUrl + path;

            HTTPRequest req = new HTTPRequest(new Uri(fullUrl), HTTPMethods.Put, callbackDelegate);
            req.AddHeader("content-type", "application/json-patch+json");
            req.AddHeader("authorization", "Bearer " + _accessToken);

            return req;
        }

        #endregion
    }
}
