﻿namespace Goontech.Anzyz_API_Client.Scripts.Models.Search
{
    public class TargetPhrasesModel
    {
        public string[] targetPhrases { get; set; }
        public string[] ignorePhrases { get; set; }
        public int maxPhrasesGenerated { get; set; }
        public bool strict { get; set; }

        public TargetPhrasesModel(string[] targetPhrases, string[] ignorePhrases, int maxPhrasesGenerated, bool strict)
        {
            this.targetPhrases = targetPhrases;
            this.ignorePhrases = ignorePhrases;
            this.maxPhrasesGenerated = maxPhrasesGenerated;
            this.strict = strict;
        }
    }
}