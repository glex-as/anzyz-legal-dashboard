﻿namespace Goontech.Anzyz_API_Client.Scripts
{
    public class DocumentResponseModel
    {
        public Document[] Documents { get; set; }

        public DocumentResponseModel(Document[] documents)
        {
            Documents = documents;
        }
    }
}