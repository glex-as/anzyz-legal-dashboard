﻿using System.Collections.Generic;
using Goontech.Anzyz_API_Client.Scripts.Models.Search;
using Newtonsoft.Json;

namespace Goontech.Anzyz_API_Client.Scripts
{
    public class CategoryRequestModel
    {
        /// <summary>
        /// Name of the Category
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// Category ID from Dashboard
        /// </summary>
        public string ext_id { get; set; }

        /// <summary>
        /// Page size; Defines the size of the result
        /// </summary>
        public int p_s { get; set; }

        /// <summary>
        /// MongoDB query
        /// </summary>
        public Dictionary<string, object> q { get; set; }

        /// <summary>
        /// Knowledge Base Name
        /// </summary>
        public string r { get; set; }

        /// <summary>
        /// Deprecated
        /// </summary>
        public bool report { get; set; }

        /// <summary>
        /// Target Phrases Model; Target phrases, ignore phrases, number to generate, strict/not strict
        /// </summary>
        public object[] ti { get; set; }

        public CategoryRequestModel(string description, string extId, int pS, Dictionary<string, object> query, bool report, TopicRequestModel requestModel)
        {
            this.description = description;
            ext_id = extId;
            p_s = pS;
            q = query;
            r = requestModel.r;
            ti = requestModel.ti;
            this.report = report;
        }
    }

    public class CategoryResponseModel
    {
        public List<Category> categories;

        public CategoryResponseModel(List<Category> categories)
        {
            this.categories = categories;
        }
    }

    public class Category
    {
        public string id { get; set; }

        /// <summary>
        /// Name of the Category
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// Category ID from Dashboard
        /// </summary>
        public string ext_id { get; set; }

        public QueryModel query { get; set; }

        /// <summary>
        /// Deprecated
        /// </summary>
        public bool report { get; set; }

        public Category(string _id, string description, string ext_Id, string r, object query, bool report)
        {
            id = _id;
            this.description = description;
            this.ext_id = ext_Id;
            this.report = report;

            string queryAsString = query.ToString();
            this.query = JsonConvert.DeserializeObject<QueryModel>(queryAsString);
        }
    }

    public class QueryModel
    {
        /// <summary>
        /// Target Phrases Model; Target phrases, ignore phrases, number to generate, strict/not strict
        /// </summary>
        public List<object[]> ti { get; set; }

        /// <summary>
        /// Page size; Defines the size of the result
        /// </summary>
        public int p_s { get; set; }

        /// <summary>
        /// Knowledge Base Name
        /// </summary>
        public string r { get; set; }


        /// <summary>
        /// dict, MongoDB query
        /// </summary>
        public object q { get; set; }

        public QueryModel(int p_s, object q, string r, List<object[]> ti)
        {
            this.p_s = p_s;
            this.q = q;
            this.r = r;
            this.ti = ti;
        }
    }
}