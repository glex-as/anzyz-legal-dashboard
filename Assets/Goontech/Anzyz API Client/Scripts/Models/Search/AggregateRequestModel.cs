﻿using Goontech.Anzyz_API_Client.Scripts.Models.Search;
using System.Collections.Generic;

namespace Goontech.Anzyz_API_Client.Scripts
{
    public class AggregateRequestModel
    {
        /// <summary>
        /// MongoDB pipeline
        /// </summary>
        public object a { get; set; }

        /// <summary>
        /// ?
        /// </summary>
        public bool invert { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int p_s { get; set; }

        /// <summary>
        /// Knowledge Base Name
        /// </summary>
        public string r { get; set; }

        /// <summary>
        /// Target phrases, ignore phrases, max phrases generated, strict
        /// </summary>
        public TargetPhrasesModel ti { get; set; }

        /// <summary>
        /// Asserts whether wave prediction should be run or not
        /// </summary>
        public bool wave_prediction { get; set; }

        public AggregateRequestModel(object a, bool invert, int pS, string r, TargetPhrasesModel ti, bool wavePrediction)
        {
            this.a = a;
            this.invert = invert;
            p_s = pS;
            this.r = r;
            this.ti = ti;
            wave_prediction = wavePrediction;
        }
    }

    public class AggregateResponseModel
    {
        public Dictionary<string, bool> keys;

        public AggregateResponseModel(Dictionary<string, bool> keys)
        {
            this.keys = keys;
        }
    }

    public class RetrieveAggregateRequestModel
    {
        public string id { get; set; }
        public bool wave_prediction { get; set; }

        public RetrieveAggregateRequestModel(string id, bool wavePrediction)
        {
            this.id = id;
            wave_prediction = wavePrediction;
        }
    }
}