﻿namespace Goontech.Anzyz_API_Client.Scripts.Models.Search
{
    public class TopicRequestModel
    {
        public string r { get; set; }

        public object[] ti { get; set; }

        public TopicRequestModel(string knowledgeBaseName, string[] targetPhrases, string[] ignorePhrases, int maxPhrasesGenerated, bool strict)
        {
            r = knowledgeBaseName;
            ti = new object[]
            {
                targetPhrases,
                ignorePhrases,
                maxPhrasesGenerated,
                strict
            };
        }
    }

    public class TopicResponseModel
    {
        public float[] count { get; set; }
        public string[] phrase { get; set; }
        public string[] relatedness { get; set; }
    }
}