﻿using System.Collections.Generic;

namespace Goontech.Anzyz_API_Client.Scripts
{
    public class Document
    {
        public int Total_Page_number { get; set; }
        public int _rand { get; set; }
        public int _rand2 { get; set; }
        public string[] category_phrases { get; set; }
        public string content { get; set; }
        public string creation_time { get; set; }
        public float day { get; set; }
        public string file_name { get; set; }
        public string file_path { get; set; }
        public float month { get; set; }
        public int page_number { get; set; }
        public string source { get; set; }
        public string[] terms { get; set; }
        public Dictionary<string, object> value_pairs { get; set; }
        public float week { get; set; }

        public Document(int Total_Page_number, int _rand, int _rand2, string[] category_phrases, string content, string creation_time, float day, string file_name, string file_path,
            float month, int page_number, string source, string[] terms, Dictionary<string, object> value_pairs, float week)
        {
            this.Total_Page_number = Total_Page_number;
            this._rand = _rand;
            this._rand2 = _rand2;
            this.category_phrases = category_phrases;
            this.content = content;
            this.creation_time = creation_time;
            this.day = day;
            this.file_name = file_name;
            this.file_path = file_path;
            this.month = month;
            this.page_number = page_number;
            this.source = source;
            this.terms = terms;

            if (value_pairs != null)
            {
                this.value_pairs = value_pairs;
            }
            this.week = week;
        }
    }
}