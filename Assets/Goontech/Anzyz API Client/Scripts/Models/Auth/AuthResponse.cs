﻿namespace Goontech.Anzyz_API_Client.Scripts.Models.Auth
{
    public class AuthResponse
    {
        public string access_token { get; set; }
        public string scope { get; set; }
        public int expires_in { get; set; }
        public string token_type { get; set; }

        public AuthResponse(string accessToken, string scope, int expiresIn, string tokenType)
        {
            access_token = accessToken;
            this.scope = scope;
            expires_in = expiresIn;
            token_type = tokenType;
        }
    }
}