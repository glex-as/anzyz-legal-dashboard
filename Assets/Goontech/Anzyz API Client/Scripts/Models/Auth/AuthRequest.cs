﻿namespace Goontech.Anzyz_API_Client.Scripts.Models.Auth
{
    public class AuthRequest
    {
        public string client_id { get; set; }
        
        public string client_secret { get; set; }

        public string audience { get; set; }

        public string grant_type { get; set; }

        public AuthRequest(string clientId, string clientSecret, string audience, string grantType)
        {
            client_id = clientId;
            client_secret = clientSecret;
            this.audience = audience;
            grant_type = grantType;
        }
    }
}