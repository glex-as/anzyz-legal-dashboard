using System;
using System.Collections.Generic;
using BestHTTP;
using Goontech_AS.Scripts.Models.Auth;
using Goontech_AS.Scripts.Models.Content;

namespace Goontech_AS.Scripts
{
    public interface IGtApiClient
    {
        event Action OnGotTenantInfo;
        event Action OnLoginSuccess;
        event Action<string> OnLoginError;
        event Action<UserInfo> OnGotUserInfo;
        event Action<byte[]> OnUserImageDownloaded;

        // Login / Authentication
        void Login(string email, string password, string tenantName, string tenantId);
        bool CanGetTenantNameFromEmail(string email);
        string GetTenantCodeFromEmail(string email);
        int GetIdNumberFromTenantCode(string tenantCode);
        string GetTenantNameFromId(int id);

        // User management
        void ResetPassword(string emailAddress, string tenancyCode, OnRequestFinishedDelegate callback);
        void GetUserInfo();
        void GetUserProfileImage();

        // Content
        void CreateContent(Guid id, string contentType, OnRequestFinishedDelegate callback);
        void GetContent(Guid id, OnRequestFinishedDelegate callback);
        void GetList(List<Guid> ids, OnRequestFinishedDelegate callback);
        void GetAllContentOfType(string contentType, OnRequestFinishedDelegate callback);
        void GetAllContent();
        void UpdateContent(Result model, OnRequestFinishedDelegate callback);
        void DeleteContent(Guid id, OnRequestFinishedDelegate callback);

        // Cosmos
        void CreateAndAddToCosmos(Guid id, string contentType, byte[] data, string fileName, OnRequestFinishedDelegate callback);
        void AddToCosmos(Guid id, string fileName, byte[] data, OnRequestFinishedDelegate callback);
        void GetListCosmosItems(List<Guid> ids, OnRequestFinishedDelegate callback);
        void GetAllCosmosItemsOfType(string contentType, OnRequestFinishedDelegate callback);
        void DeleteCosmosItem(Guid id, string contentType, string fileName, OnRequestFinishedDelegate callback);

        // Files and attachments

        void GetFile(Guid id, string fileName, OnRequestFinishedDelegate callback);
        void UploadFileToBlob(Guid id, string filePath, OnRequestFinishedDelegate callback);
        void UploadFileToBlob(Guid id, byte[] fileAsBytes, string fileName, OnRequestFinishedDelegate callback);
        void GetDownloadUrl(Guid id, string fileName, OnRequestFinishedDelegate callback);
        void GetThumbnailFromUrl(string url, int targetWidth, OnRequestFinishedDelegate callback);
        void GetContentFromUrl(string url, OnRequestFinishedDelegate callback);

        // Search and Filter
        void Search(QueryParameterDto[] queryParameters, OnRequestFinishedDelegate callback);
        void Filter(QueryParameterDto[] queryParameters, OnRequestFinishedDelegate callback);

        // Permissions
        void ToggleUseDataAccessLayer(bool use);
    }
}