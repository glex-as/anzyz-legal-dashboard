using System;

namespace Goontech_AS.Scripts.Models.Auth
{
    [Serializable]
    public class GetProfileImageModel
    {
        public Result result { get; set; }
        public object targetUrl { get; set; }
        public bool success { get; set; }
        public object error { get; set; }
        public bool unAuthorizedRequest { get; set; }
        public bool __abp { get; set; }

        public class Result
        {
            public string profilePicture { get; set; }
        }
    }
}