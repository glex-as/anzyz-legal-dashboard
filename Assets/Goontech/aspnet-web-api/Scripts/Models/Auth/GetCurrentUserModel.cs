using System;

namespace Goontech_AS.Scripts.Models.Auth
{
    [Serializable]
    public class GetCurrentUserModel
    {
        public Result result { get; set; }
        public object targetUrl { get; set; }
        public bool success { get; set; }
        public object error { get; set; }
        public bool unAuthorizedRequest { get; set; }
        public bool __abp { get; set; }

        public class Result
        {
            public string name { get; set; }
            public string surname { get; set; }
            public string userName { get; set; }
            public string emailAddress { get; set; }
            public string phoneNumber { get; set; }
            public bool isPhoneNumberConfirmed { get; set; }
            public string timezone { get; set; }
            public string qrCodeSetupImageUrl { get; set; }
            public bool isGoogleAuthenticatorEnabled { get; set; }
        }
    }
}