using System;

namespace Goontech_AS.Scripts.Models.Auth
{
    [Serializable]
    public class TenantInfo
    {
        public string tenantCode { get; set; }
        public int id { get; set; }
        public string[] domains { get; set; }
        public string name { get; set; }
    }
}