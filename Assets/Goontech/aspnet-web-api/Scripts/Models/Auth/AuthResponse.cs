﻿using System;

namespace Goontech_AS.Scripts.Models.Auth
{
    [Serializable]
    public class AuthResponse
    {
        public Results Result;
        public string TargetUrl;
        public bool Success;
        public string Error;
        public bool UnAuthorizedRequest;
        public bool Abp;

        public struct Results
        {
            public string accessToken;
            public int expireInSeconds;
            public int userId;
        }
    }
}
