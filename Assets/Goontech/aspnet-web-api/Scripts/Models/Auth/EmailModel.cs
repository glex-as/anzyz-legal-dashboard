using System;

namespace Goontech_AS.Scripts.Models.Auth
{
    [Serializable]
    public class EmailModel
    {
        public string emailAddress { get; set; }
    }
}