using System;

namespace Goontech_AS.Scripts.Models.Auth
{
    [Serializable]
    public class AuthRequest
    {
        public string UserNameOrEmailAddress;
        public string Password;
        public string TenancyName;
        public bool RememberClient;

        public AuthRequest()
        {

        }

        public AuthRequest(string userNameOrEmailAddress, string password, string tenancyName, bool rememberClient)
        {
            UserNameOrEmailAddress = userNameOrEmailAddress;
            Password = password;
            TenancyName = tenancyName;
            RememberClient = rememberClient;
        }
    }
}
