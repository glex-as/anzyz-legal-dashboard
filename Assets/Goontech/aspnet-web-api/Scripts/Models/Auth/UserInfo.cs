using System;

namespace Goontech_AS.Scripts.Models.Auth
{
    [Serializable]
    public class UserInfo
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string Timezone { get; set; }

        public UserInfo(string name, string surname, string userName, string emailAddress, string phoneNumber, string timezone)
        {
            Name = name;
            Surname = surname;
            UserName = userName;
            EmailAddress = emailAddress;
            PhoneNumber = phoneNumber;
            Timezone = timezone;
        }
    }
}