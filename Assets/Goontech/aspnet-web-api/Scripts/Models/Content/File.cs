namespace Goontech_AS.Scripts.Models.Content
{
    public class File
    {
        public string fileName { get; set; }
        public string url { get; set; }

        public override string ToString() => fileName;
    }
}