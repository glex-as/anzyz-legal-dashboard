namespace Goontech_AS.Scripts.Models.Content
{
    public class SearchModel
    {
        public string contentType;
        public string property;
        public string Operator;
        public string value;

        public SearchModel(string contentType, string property, string @operator, string value)
        {
            this.contentType = contentType;
            this.property = property;
            Operator = @operator;
            this.value = value;
        }
    }
}