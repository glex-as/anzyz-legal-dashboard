namespace Goontech_AS.Scripts.Models.Content
{
    public class CreateContentModel
    {
        public string Id;
        public string ContentType;
        public bool IsPublished { get; set; }
        public bool IsPublic { get; set; }

        public CreateContentModel(string id, string contentType, bool isPublished = false, bool isPublic = false)
        {
            Id = id;
            ContentType = contentType;
            IsPublished = isPublished;
            IsPublic = isPublic;
        }
    }
}
