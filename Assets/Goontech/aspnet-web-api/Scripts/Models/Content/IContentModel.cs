using System;

namespace Goontech_AS.Scripts.Models.Content
{
    public interface IContentModel
    {
        string ContentType { get; }

        Guid ContentId { get; }
    }

    public class ContentModel : IContentModel
    {
        public string ContentType { get; }
        public Guid ContentId { get; }

        public ContentModel(string contentType, Guid id)
        {
            ContentType = contentType;
            ContentId = id;
        }
    }
}