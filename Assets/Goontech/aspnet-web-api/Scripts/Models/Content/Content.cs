using System.Collections.Generic;

namespace Goontech_AS.Scripts.Models.Content
{
    public class Content
    {
        public string Id;
        public List<ContentFile> Files;

        public Content(string id)
        {
            Id = id;
            Files = new List<ContentFile>();
        }

    }
}