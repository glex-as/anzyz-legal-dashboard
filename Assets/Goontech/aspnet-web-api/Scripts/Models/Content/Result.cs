using System;

namespace Goontech_AS.Scripts.Models.Content
{
    public class Result
    {
        public string id { get; set; }
        public string contentType { get; set; }
        public object url { get; set; }
        public File[] files { get; set; }
        public DateTime lastExecutionTime { get; set; }
        public object trigger { get; set; }
        public bool isPublished { get; set; }
        public bool isPublic { get; set; }
    }
}