namespace Goontech_AS.Scripts.Models.Content
{
    public class ContentFile
    {
        public string FileName;
        public string Url;

        public ContentFile(string fileName, string url)
        {
            FileName = fileName;
            Url = url;
        }
    }
}