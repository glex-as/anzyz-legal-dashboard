namespace Goontech_AS.Scripts.Models.Content
{
    public class QueryParameterDto
    {
        public string LogicalOperator { get; }

        public string PropertyName { get; }

        public string ComparisonOperator { get; }

        public object Value { get; }

        public QueryParameterDto[] SubQuery { get; }

        public QueryParameterDto(string logicalOperator, string propertyName, string comparisonOperator, object value)
        {
            LogicalOperator = logicalOperator;
            PropertyName = propertyName;
            ComparisonOperator = comparisonOperator;
            Value = value;
        }

        public QueryParameterDto(string logicalOperator, QueryParameterDto[] subQuery)
        {
            LogicalOperator = logicalOperator;
            SubQuery = subQuery;
        }
    }
}