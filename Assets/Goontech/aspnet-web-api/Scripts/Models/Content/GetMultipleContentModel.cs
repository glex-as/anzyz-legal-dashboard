namespace Goontech_AS.Scripts.Models.Content
{
    public class GetMultipleContentModel
    {
        public Result[] result { get; set; }
        public object targetUrl { get; set; }
        public bool success { get; set; }
        public object error { get; set; }
        public bool unAuthorizedRequest { get; set; }
        public bool __abp { get; set; }
    }
}