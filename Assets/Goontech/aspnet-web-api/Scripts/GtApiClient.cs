using BestHTTP;
using Goontech_AS.Scripts.Models.Auth;
using Goontech_AS.Scripts.Models.Content;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Goontech_AS.Scripts
{
    public class GtApiClient : IGtApiClient
    {
        #region References specific to Project:

        private readonly string _baseUrl;
        private readonly string _tenantInfoUrl;

        #endregion

        public event Action OnGotTenantInfo;
        public event Action OnLoginSuccess;
        public event Action<string> OnLoginError;
        public event Action<UserInfo> OnGotUserInfo;
        public event Action<byte[]> OnUserImageDownloaded;

        private string _accessToken = "";

        private List<TenantInfo> _tenantInfo;
        private string _tenantName;
        private string _tenantIdNumber;

        public GtApiClient(string baseUrl, string tenantInfoUrl)
        {
            _baseUrl = baseUrl;
            _tenantInfoUrl = tenantInfoUrl;

            _tenantInfo = new List<TenantInfo>();

            GetTenantInfo();

        }

        #region Authentication

        public void Login(string email, string password, string tenantName, string tenantId)
        {
            _tenantName = tenantName;
            _tenantIdNumber = tenantId;

            AuthRequest authRequest = new AuthRequest(email, password, _tenantName, true);

            string json = JsonConvert.SerializeObject(authRequest);

            HTTPRequest req = new HTTPRequest(new Uri(_baseUrl + "/api/TokenAuth/Authenticate"), HTTPMethods.Post, OnLoginComplete);
            req.AddHeader("content-type", "application/json");
            req.AddHeader("Abp.TenantId", _tenantIdNumber);
            req.RawData = new UTF8Encoding().GetBytes(json);
            req.Send();
        }

        private void OnLoginComplete(HTTPRequest request, HTTPResponse response)
        {
            if (response.IsSuccess)
            {
                AuthResponse authResponse = JsonConvert.DeserializeObject<AuthResponse>(response.DataAsText);

                if (authResponse.UnAuthorizedRequest)
                {
                    OnLoginError?.Invoke("Login failed. You might need to set a new password.");
                }
                else
                {
                    _accessToken = authResponse.Result.accessToken;

                    OnLoginSuccess?.Invoke();
                }
            }

            else
            {
                OnLoginError?.Invoke($"Login failed with message: {response.Message} and status code: {response.StatusCode}");
            }
        }

        private void GetTenantInfo()
        {
            var req = new HTTPRequest(new Uri(_tenantInfoUrl), HTTPMethods.Get, OnGotTenantInfoComplete);
            req.Send();
        }

        private void OnGotTenantInfoComplete(HTTPRequest request, HTTPResponse response)
        {
            if (response.IsSuccess)
            {
                _tenantInfo = JsonConvert.DeserializeObject<List<TenantInfo>>(response.DataAsText);

                OnGotTenantInfo?.Invoke();
            }

            else
            {
                OnLoginError?.Invoke($"Failed to get Tenant Info.");
            }
        }

        public bool CanGetTenantNameFromEmail(string email)
        {
            foreach (TenantInfo tenantInfo in _tenantInfo)
            {
                foreach (string domain in tenantInfo.domains)
                {
                    if (domain != string.Empty)
                    {
                        if (email.Contains(domain))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public string GetTenantCodeFromEmail(string email)
        {
            if (_tenantInfo != null)
            {
                foreach (TenantInfo tenantInfo in _tenantInfo)
                {
                    foreach (string domain in tenantInfo.domains)
                    {
                        if (domain != string.Empty && email.Contains(domain))
                        {
                            return tenantInfo.tenantCode;
                        }
                    }
                }
            }

            return "";
        }

        public int GetIdNumberFromTenantCode(string tenantCode)
        {
            int id = -5;

            if (_tenantInfo.Any(x => x.tenantCode.Equals(tenantCode)))
            {
                TenantInfo first = null;
                foreach (var x in _tenantInfo)
                {
                    if (x.tenantCode.Equals(tenantCode))
                    {
                        first = x;
                        break;
                    }
                }

                if (first != null) id = first.id;
            }

            return id;
        }

        public string GetTenantNameFromId(int id)
        {
            string tenantName = "";

            if (_tenantInfo.Any(x => x.id.Equals(id)))
            {
                tenantName = _tenantInfo.FirstOrDefault(x => x.id.Equals(id))?.name;
            }

            return tenantName;
        }

        #endregion

        #region User / profile

        public void ResetPassword(string emailAddress, string tenancyCode, OnRequestFinishedDelegate callback)
        {
            int id = GetIdNumberFromTenantCode(tenancyCode);

            EmailModel email = new EmailModel();
            email.emailAddress = emailAddress;
            string emailText = JsonConvert.SerializeObject(email);

            HTTPRequest req = NewPostHttpRequest("/api/services/app/Account/SendPasswordResetCode", callback);
            req.AddHeader("content-type", "application/json");
            req.AddHeader("Abp.TenantId", id.ToString());
            req.RawData = new UTF8Encoding().GetBytes(emailText);
            req.Send();
        }

        public void GetUserInfo()
        {
            HTTPRequest req = NewGetHttpRequest("/api/services/app/Profile/GetCurrentUserProfileForEdit", OnGetUserInfoCompleted);
            req.Send();
        }

        private void OnGetUserInfoCompleted(HTTPRequest request, HTTPResponse response)
        {
            if (response != null)
            {
                if (response.IsSuccess)
                {
                    GetCurrentUserModel infoModel = JsonConvert.DeserializeObject<GetCurrentUserModel>(response.DataAsText);

                    UserInfo info = new UserInfo(infoModel.result.name, infoModel.result.surname, infoModel.result.userName, infoModel.result.emailAddress, infoModel.result.phoneNumber, infoModel.result.timezone);

                    OnGotUserInfo?.Invoke(info);
                }
            }
        }

        public void GetUserProfileImage()
        {
            HTTPRequest req = NewGetHttpRequest("/api/services/app/Profile/GetProfilePicture", OnGetProfilePictureCompleted);
            req.Send();
        }

        private void OnGetProfilePictureCompleted(HTTPRequest request, HTTPResponse response)
        {
            if (response.IsSuccess)
            {
                GetProfileImageModel responseData = JsonConvert.DeserializeObject<GetProfileImageModel>(response.DataAsText);

                if (!responseData.result.profilePicture.Equals(string.Empty))
                {
                    byte[] bytes = Convert.FromBase64String(responseData.result.profilePicture);

                    OnUserImageDownloaded?.Invoke(bytes);
                }
            }
        }

        #endregion

        #region Content

        private void CreateIfNotExist(Guid id, string contentType)
        {
            GetContent(id, (req, res) =>
            {
                if (res != null)
                {
                    if (res.IsSuccess)
                    {
                        GetSingleContentModel root = JsonConvert.DeserializeObject<GetSingleContentModel>(res.DataAsText);

                        if (root.success)
                        {
                            Debug.Log($"Content with id {id} already exists. Skipping Create.");
                        }
                    }
                    else
                    {
                        CreateContent(id, contentType, (req2, res2) =>
                        {
                            Debug.Log($"Create request for {contentType} with ContentId {id} returned with status code {res2.StatusCode} and message {res2.Message}.");
                        });
                    }
                }
            });
        }

        public void CreateContent(Guid id, string contentType, OnRequestFinishedDelegate callback)
        {
            CreateContentModel doc = new CreateContentModel(id.ToString(), contentType);
            string json = JsonConvert.SerializeObject(doc);

            HTTPRequest req = NewPostHttpRequest("/api/services/app/Content/Create", callback);
            req.RawData = new UTF8Encoding().GetBytes(json);
            req.Send();
        }

        /// <summary>
        /// Creates a Content if it doesn't already exist, then uploads the given data to CosmosDb using AddToCosmos method
        /// </summary>
        /// <param name="id"></param>
        /// <param name="contentType"></param>
        /// <param name="data"></param>
        /// <param name="fileName"></param>
        /// <param name="callback"></param>
        public void CreateAndAddToCosmos(Guid id, string contentType, byte[] data, string fileName, OnRequestFinishedDelegate callback)
        {
            CreateIfNotExist(id, contentType);

            AddToCosmos(id, fileName, data, callback);
        }

        /// <summary>
        /// Adds a file to CosmosDB. Requires a valid and existing ContentId, a fileName and the file content as a byte array. 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fileName"></param>
        /// <param name="data"></param>
        /// <param name="callback"></param>
        public void AddToCosmos(Guid id, string fileName, byte[] data, OnRequestFinishedDelegate callback)
        {
            string fullUrl = _baseUrl + $"api/services/app/Content/AddToCosmos?Id={id}&fileName={fileName}";

            HTTPRequest req = new HTTPRequest(new Uri(fullUrl), HTTPMethods.Post, callback);
            req.AddHeader("Abp.TenantId", _tenantIdNumber);
            req.AddHeader("authorization", "Bearer " + _accessToken);

            req.AddBinaryData("file", data, fileName, "multipart/form-data");

            req.Send();
        }

        /// <summary>
        /// Returns a list of JObject based on the list of ContentId's passed in 
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="callback"></param>
        public void GetListCosmosItems(List<Guid> ids, OnRequestFinishedDelegate callback)
        {
            var req = NewPostHttpRequest("/api/services/app/Content/CosmosItemsList", callback);

            string idsAsJson = JsonConvert.SerializeObject(ids);
            byte[] idsAsBytes = Encoding.UTF8.GetBytes(idsAsJson);
            req.RawData = idsAsBytes;

            req.Send();
        }

        /// <summary>
        /// Get's all items in Cosmos DB with the specified ContentType and Filter values (property, operator and value) and returns them as a Json string.
        /// If Filter values is left blank it will only use the ContentType, and if ContentType is left blank it will only use the Filter values
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="callback">The event to be triggered on response.</param>
        public void GetAllCosmosItemsOfType(string contentType, OnRequestFinishedDelegate callback)
        {
            var req = NewGetHttpRequest($"/api/services/app/Content/GetAllCosmosItemsOfType?contentType={contentType}", callback).Send();
        }

        /// <summary>
        /// Deletes the Content with the given Id, and all files uploaded to this Content
        /// </summary>
        /// <param name="id"></param>
        /// <param name="callback"></param>
        public void DeleteContent(Guid id, OnRequestFinishedDelegate callback)
        {
            HTTPRequest req = NewDeleteHttpRequest($"/api/services/app/Content/Delete?Id={id}", callback);
            req.Send();
        }

        /// <summary>
        /// Deletes the items with the given ContentId, ContentType and Filename from CosmosDB
        /// </summary>
        /// <param name="id"></param>
        /// <param name="contentType"></param>
        /// <param name="fileName"></param>
        /// <param name="callback"></param>
        public void DeleteCosmosItem(Guid id, string contentType, string fileName, OnRequestFinishedDelegate callback)
        {
            HTTPRequest req = NewDeleteHttpRequest($"/api/services/app/Content/DeleteCosmosFile?Id={id}&contentType={contentType}&fileName={fileName}", callback);
            req.Send();
        }

        /// <summary>
        /// Get's a File
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fileName"></param>
        /// <param name="callback"></param>
        public void GetFile(Guid id, string fileName, OnRequestFinishedDelegate callback)
        {
            HTTPRequest req = NewGetHttpRequest($"/api/services/app/Content/GetFile?id={id}&fileName={fileName}", callback);
            req.Send();
        }


        public void GetAvailableDataSources(OnRequestFinishedDelegate callback)
        {
            var req = NewGetHttpRequest("/api/services/app/Content/GetAllContentOfType?ContentType=DataSource", callback);
            req.Send();
        }


        /// <summary>
        /// Returns a list of Content based on the list of ContentId's passed in
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="callback"></param>
        public void GetList(List<Guid> ids, OnRequestFinishedDelegate callback)
        {
            var req = NewPostHttpRequest($"/api/services/app/Content/ContentList", callback);

            string idsAsJson = JsonConvert.SerializeObject(ids);
            byte[] idsAsBytes = Encoding.UTF8.GetBytes(idsAsJson);
            req.RawData = idsAsBytes;

            req.Send();
        }

        /// <summary>
        /// Get's all Content of a certain ContentType
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="callback"></param>
        public void GetAllContentOfType(string contentType, OnRequestFinishedDelegate callback)
        {
            var req = NewGetHttpRequest($"/api/services/app/Content/GetAllContentOfType?ContentType={contentType}", callback);
            req.Send();
        }

        /// <summary>
        /// Get's a single Content from a ContentId
        /// </summary>
        /// <param name="id"></param>
        /// <param name="callback"></param>
        public void GetContent(Guid id, OnRequestFinishedDelegate callback)
        {
            var req = NewGetHttpRequest($"/api/services/app/Content/Get?id={id}", callback);
            req.Send();
        }

        public void GetAllContent()
        {
            HTTPRequest getContentReq = NewGetHttpRequest("/api/services/app/Content/GetAll", OnGetAllContentComplete);
            getContentReq.Send();
        }

        private void OnGetAllContentComplete(HTTPRequest request, HTTPResponse response)
        {
            Debug.Log(response.DataAsText);

            JObject result = JObject.Parse(response.DataAsText);

            List<Content> contentList = new List<Content>();

            foreach (var token in result.Properties().First().Value)
            {
                if (token != null)
                {
                    string id = token["id"].ToString();
                    Content cont = new Content(id);

                    if (token["files"].Any())
                    {
                        foreach (var file in token["files"])
                        {
                            string fileName = file["fileName"].ToString();
                            string url = file["url"].ToString();

                            cont.Files.Add(new ContentFile(fileName, url));
                        }
                    }

                    contentList.Add(cont);
                }
            }

            foreach (var cont in contentList)
            {
                foreach (var file in cont.Files)
                {
                    Debug.Log(file.FileName);
                }
            }
        }

        /// <summary>
        /// Get's content from a full url (NOT pre-formatted API url)
        /// </summary>
        /// <param name="url"></param>
        /// <param name="callback"></param>
        public void GetContentFromUrl(string url, OnRequestFinishedDelegate callback)
        {
            HTTPRequest req = new HTTPRequest(new Uri(url), HTTPMethods.Get, callback).Send();
        }

        /// <summary>
        /// Get's all items in Cosmos DB that matches the criteria and returns them as Content
        /// </summary>
        /// <param name="queryParameters"></param>
        /// <param name="callback"></param>
        public void Search(QueryParameterDto[] queryParameters, OnRequestFinishedDelegate callback)
        {
            string json = JsonConvert.SerializeObject(queryParameters);

            var req = NewPostHttpRequest("/api/services/app/Content/Search", callback);

            req.RawData = new UTF8Encoding().GetBytes(json);

            req.Send();
        }

        public void GetDownloadUrl(Guid id, string fileName, OnRequestFinishedDelegate callback)
        {
            HTTPRequest req = NewGetHttpRequest($"/api/services/app/Content/GetDownloadUrl?Id={id}&fileName={fileName}", callback).Send();
        }


        /// <summary>
        /// Get's all items in Cosmos DB with the specified ContentType and Filter values (property, operator and value) and returns them as a Json string.
        /// If Filter values is left blank it will only use the ContentType, and if ContentType is left blank it will only use the Filter values
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="property"></param>
        /// <param name="operator"></param>
        /// <param name="value"></param>
        /// <param name="callback">The event to be triggered on response.</param>
        public void GetAllCosmosItemsOfType(string contentType, string property, string @operator, string value, OnRequestFinishedDelegate callback)
        {
            var req = NewGetHttpRequest($"/api/services/app/Content/GetAllCosmosItemsOfType?contentType={contentType}&property={property}&Operator={@operator}&value={value}", callback).Send();

            //SearchModel search = new SearchModel(contentType, "", "", "");
            //string json = JsonConvert.SerializeObject(search);
            //var req = NewPostHttpRequest($"/api/services/app/Content/Search?contentType={contentType}", callback);
            //req.RawData = new UTF8Encoding().GetBytes(json);
            //req.Send();
        }


        /// <summary>
        /// Get's an image from the url resized to the specified target width
        /// </summary>
        /// <param name="url"></param>
        /// <param name="targetWidth"></param>
        /// <param name="callback"></param>
        public void GetThumbnailFromUrl(string url, int targetWidth, OnRequestFinishedDelegate callback)
        {
            var req = NewGetHttpRequest($"/api/services/app/Content/GetThumbnailFromUrl?Uri={url}&TargetWidth={targetWidth}", callback).Send();
        }

        /// <summary>
        /// Get's all items in Cosmos DB that matches the criteria and returns them as Content
        /// </summary>
        /// <param name="value"></param>
        /// <param name="callback"></param>
        /// <param name="propertyName"></param>
        /// <param name="operatorName"></param>
        public void SearchCosmosItems(string propertyName, string operatorName, string value, OnRequestFinishedDelegate callback)
        {
            SearchModel search = new SearchModel("", propertyName, operatorName, value);
            string json = JsonConvert.SerializeObject(search);

            var req = NewPostHttpRequest($"/api/services/app/Content/Search?property={propertyName}&Operator={operatorName}&value={value}", callback);

            req.RawData = new UTF8Encoding().GetBytes(json);

            req.Send();
        }

        /// <summary>
        /// Get's all items in Cosmos DB that matches the criterias and returns them as a list of Guid
        /// </summary>
        /// <param name="queryParameters"></param>
        /// <param name="callback"></param>
        public void Filter(QueryParameterDto[] queryParameters, OnRequestFinishedDelegate callback)
        {
            string json = JsonConvert.SerializeObject(queryParameters);

            var req = NewPostHttpRequest($"/api/services/app/Content/Filter", callback);

            req.RawData = new UTF8Encoding().GetBytes(json);

            req.Send();
        }

        /// <summary>
        /// Upload a file from disk to Blob Storage with it's current name
        /// </summary>
        /// <param name="id"></param>
        /// <param name="filePath"></param>
        /// <param name="callback"></param>
        public void UploadFileToBlob(Guid id, string filePath, OnRequestFinishedDelegate callback)
        {
            string fileName = Path.GetFileName(filePath);
            var fileAsBytes = System.IO.File.ReadAllBytes(filePath);

            UploadFileToBlob(id, fileAsBytes, fileName, callback);
        }

        /// <summary>
        /// Upload a file from disk to Blob Storage with a new filename
        /// </summary>
        /// <param name="id"></param>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        /// <param name="callback"></param>
        public void UploadFileToBlob(Guid id, string filePath, string fileName, OnRequestFinishedDelegate callback)
        {
            var fileAsBytes = System.IO.File.ReadAllBytes(filePath);

            UploadFileToBlob(id, fileAsBytes, fileName, callback);
        }

        /// <summary>
        /// Upload a file as byte array to Blob Storage
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fileAsBytes"></param>
        /// <param name="fileName"></param>
        /// <param name="callback"></param>
        public void UploadFileToBlob(Guid id, byte[] fileAsBytes, string fileName, OnRequestFinishedDelegate callback)
        {
            string fullUrl = _baseUrl + "/api/services/app/Content/Upload";

            HTTPRequest req = new HTTPRequest(new Uri(fullUrl), HTTPMethods.Post, callback);
            req.AddHeader("Abp.TenantId", _tenantIdNumber);
            req.AddHeader("authorization", "Bearer " + _accessToken);

            req.AddField("Id", id.ToString());

            req.AddBinaryData("file", fileAsBytes, fileName, "multipart/form-data");

            req.Send();
        }

        public void UpdateContent(Result model, OnRequestFinishedDelegate callback)
        {
            string json = JsonConvert.SerializeObject(model);

            HTTPRequest req = NewPutHttpRequest("/api/services/app/Content/Update", callback);
            req.RawData = new UTF8Encoding().GetBytes(json);
            req.Send();
        }

        #endregion

        

        #region Permissions

        public void ToggleUseDataAccessLayer(bool use)
        {
            if (use)
            {
                var useRequest = NewPostHttpRequest("api/services/app/Content/EnableContentPermission", ((request, response) => Debug.Log(response.Message))).Send();
            }
            else
            {
                var useRequest = NewPostHttpRequest("api/services/app/Content/DisableContentPermission", ((request, response) => Debug.Log(response.Message))).Send();
            }
        }

        #endregion

        #region Generic HTTP requests

        /// <summary>
        /// GET request with Authorization headers and base uri included. Only needs a delegate and path.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="callbackDelegate"></param>
        /// <returns></returns>
        public HTTPRequest NewGetHttpRequest(string path, OnRequestFinishedDelegate callbackDelegate)
        {
            string fullUrl = _baseUrl + path;

            HTTPRequest req = new HTTPRequest(new Uri(fullUrl), HTTPMethods.Get, callbackDelegate);
            req.AddHeader("content-type", "application/json");
            req.AddHeader("Abp.TenantId", _tenantIdNumber);
            req.AddHeader("authorization", "Bearer " + _accessToken);

            return req;
        }

        /// <summary>
        /// POST request with Authorization headers and base uri included. Only needs a delegate and path.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="callbackDelegate"></param>
        /// <returns></returns>
        public HTTPRequest NewPostHttpRequest(string path, OnRequestFinishedDelegate callbackDelegate)
        {
            string fullUrl = _baseUrl + path;

            HTTPRequest req = new HTTPRequest(new Uri(fullUrl), HTTPMethods.Post, callbackDelegate);
            req.AddHeader("content-type", "application/json");
            req.AddHeader("Abp.TenantId", _tenantIdNumber);
            req.AddHeader("authorization", "Bearer " + _accessToken);

            return req;
        }

        /// <summary>
        /// DELETE request with Authorization headers and base uri included. Only needs a delegate and path.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="callbackDelegate"></param>
        /// <returns></returns>
        public HTTPRequest NewDeleteHttpRequest(string path, OnRequestFinishedDelegate callbackDelegate)
        {
            string fullUrl = _baseUrl + path;

            HTTPRequest req = new HTTPRequest(new Uri(fullUrl), HTTPMethods.Delete, callbackDelegate);
            req.AddHeader("content-type", "application/json");
            req.AddHeader("Abp.TenantId", _tenantIdNumber);
            req.AddHeader("authorization", "Bearer " + _accessToken);

            return req;
        }

        /// <summary>
        /// UPDATE request with Authorization headers and base uri included. Only needs a delegate and path.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="callbackDelegate"></param>
        /// <returns></returns>
        public HTTPRequest NewPutHttpRequest(string path, OnRequestFinishedDelegate callbackDelegate)
        {
            string fullUrl = _baseUrl + path;

            HTTPRequest req = new HTTPRequest(new Uri(fullUrl), HTTPMethods.Put, callbackDelegate);
            req.AddHeader("content-type", "application/json-patch+json");
            req.AddHeader("Abp.TenantId", _tenantIdNumber);
            req.AddHeader("authorization", "Bearer " + _accessToken);

            return req;
        }

        #endregion


    }
}