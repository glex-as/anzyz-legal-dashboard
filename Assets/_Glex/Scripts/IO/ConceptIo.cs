﻿using System;
using System.Collections.Generic;
using System.Linq;
using Glex.Core;
using Glex.IO.Serializable;
using Newtonsoft.Json;
using UnityEngine;

namespace Glex.IO
{
    public static class ConceptIo
    {
        private static string _conceptsKey = "stored_concepts";

        public static void SaveConcept(ConceptPresModel concept)
        {
            SerializableConcept conceptIo = new SerializableConcept(concept);
            string conceptString = JsonConvert.SerializeObject(conceptIo);
            PlayerPrefs.SetString(concept.ContentId.ToString(), conceptString);
            PlayerPrefs.Save();

            AddConceptToList(concept.ContentId);
        }

        private static void AddConceptToList(Guid conceptId)
        {
            if (PlayerPrefs.HasKey(_conceptsKey))
            {
                var conceptsString = PlayerPrefs.GetString(_conceptsKey);

                List<Guid> ids = JsonConvert.DeserializeObject<List<Guid>>(conceptsString);

                if (!ids.Contains(conceptId))
                {
                    ids.Add(conceptId);

                    var idsAsString = JsonConvert.SerializeObject(ids);

                    PlayerPrefs.SetString(_conceptsKey, idsAsString);
                }
            }
            else
            {
                List<Guid> ids = new List<Guid>() {conceptId};

                var idsAsString = JsonConvert.SerializeObject(ids);

                PlayerPrefs.SetString(_conceptsKey, idsAsString);
            }
        }

        public static void DeleteConcept(Guid conceptId)
        {
            PlayerPrefs.DeleteKey(conceptId.ToString());

            if (PlayerPrefs.HasKey(_conceptsKey))
            {
                var conceptsString = PlayerPrefs.GetString(_conceptsKey);

                List<Guid> ids = JsonConvert.DeserializeObject<List<Guid>>(conceptsString);

                ids.Remove(conceptId);

                var idsAsString = JsonConvert.SerializeObject(ids);

                PlayerPrefs.SetString(_conceptsKey, idsAsString);
            }
        }

        public static List<ConceptPresModel> LoadConcepts()
        {
            if (PlayerPrefs.HasKey(_conceptsKey))
            {
                var conceptsString = PlayerPrefs.GetString(_conceptsKey);

                List<Guid> ids = JsonConvert.DeserializeObject<List<Guid>>(conceptsString);

                List<ConceptPresModel> list = new List<ConceptPresModel>();

                foreach (var id in ids.Distinct())
                {
                    var conceptString = PlayerPrefs.GetString(id.ToString());

                    if (conceptString != string.Empty)
                    {
                        SerializableConcept conceptIo = JsonConvert.DeserializeObject<SerializableConcept>(conceptString);
                        ConceptPresModel concept = new ConceptPresModel(conceptIo);
                        list.Add(concept);
                    }
                }

                return list;
            }
            else
            {
                return new List<ConceptPresModel>();
            }
        }
    }
}