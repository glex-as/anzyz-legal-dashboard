﻿using Glex.Core;
using System.IO;
using Glex.Core.Managers;

namespace Glex.IO
{
    public static class EmailIo
    {
        public static EmailPresModel LoadEmailWithFilePath(string filePath)
        {
            var fixedPath = CleanString(filePath);

            var file = File.ReadAllText(fixedPath);

            //string file = Resources.Load<TextAsset>(filePath).ToString();

            // Parse as email
            EmailPresModel email = new EmailPresModel(file);

            return email;
        }

        private static string CleanString(string filePath)
        {
            string editString = filePath.Replace("/Users/haakon/Downloads/maildir/", MainManager.Instance.EnronDataBaseFolder);

            if (!editString.EndsWith(".txt"))
            {
                if (editString.EndsWith("."))
                {
                    editString += "txt";
                }
                else
                {
                    editString += ".txt";
                }
            }

            return editString;
        }
    }
}