﻿using System;
using Glex.IO.Serializable;
using Newtonsoft.Json;
using UnityEngine;

namespace Glex.IO
{
    public static class SettingsIo
    {
        private static string _settingsKey = "stored_settings";

        private static string EmptySettings => "";

        public static void SaveSettings(string filePath, int maxPhrases, int pageSize, int rand)
        {
            SerializableSettings settings = new SerializableSettings(filePath, maxPhrases, pageSize, rand);
            string settingsString = JsonConvert.SerializeObject(settings);

            PlayerPrefs.SetString(_settingsKey, settingsString);
            PlayerPrefs.Save();
        }

        public static SerializableSettings LoadSettings()
        {
            string settingsString = PlayerPrefs.GetString(_settingsKey, EmptySettings);

            SerializableSettings settings = new SerializableSettings("", 100, 50, 1);

            if (settingsString != EmptySettings)
            {
                try
                {
                    settings = JsonConvert.DeserializeObject<SerializableSettings>(settingsString);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return settings;
                }
                
            }

            return settings;
        }
    }
}