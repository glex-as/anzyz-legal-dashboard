﻿using Glex.Core;
using Newtonsoft.Json;
using System;

namespace Glex.IO.Serializable
{
    [Serializable]
    public class SerializableConcept
    {
        public Guid ContentId;
        public string Name;
        public string Description;
        public string InitialSearchWord;
        public string[] SearchTerms;
        public string[] IgnoreTerms;

        public string[] FilePaths;

        [JsonConstructor]
        public SerializableConcept(Guid contentId, string name, string description, string initialSearchWord, string[] searchTerms, string[] ignoreTerms, string[] filePaths)
        {
            ContentId = contentId;
            Name = name;
            Description = description;
            InitialSearchWord = initialSearchWord;
            SearchTerms = searchTerms;
            IgnoreTerms = ignoreTerms;

            FilePaths = filePaths ?? new string[0];
        }

        public SerializableConcept(ConceptPresModel concept)
        {
            ContentId = concept.ContentId;
            Name = concept.Name;
            Description = concept.Description;
            InitialSearchWord = concept.InitialSearchWord;
            SearchTerms = concept.SearchTerms;
            IgnoreTerms = concept.IgnoreTerms;
            FilePaths = concept.FilePaths.ToArray();
        }
    }
}