﻿namespace Glex.IO.Serializable
{
    public class SerializableSettings
    {
        public string EnronBasePath;
        public int MaxPhrases;
        public int PageSize;
        public int Rand;

        public SerializableSettings(string enronBasePath, int maxPhrases, int pageSize, int rand)
        {
            EnronBasePath = enronBasePath;
            MaxPhrases = maxPhrases;
            PageSize = pageSize;
            Rand = rand;
        }
    }
}