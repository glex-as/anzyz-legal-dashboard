﻿using System;

namespace Glex.IO.Serializable
{
    public class SerializableProject
    {
        public Guid ContentId;
        public string Name;
        public string Description;
        public string[] UserNames;

        public SerializableProject(Guid contentId, string name, string description, string[] userNames)
        {
            ContentId = contentId;
            Name = name;
            Description = description;
            UserNames = userNames;
        }
    }
}