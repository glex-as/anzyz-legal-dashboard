﻿using System;

namespace Glex.Core
{
    public class DocumentPresModel
    {
        public string RawText;
        public string[] CategoryPhrases;
        public string FileName;
        public string FilePath;
        public string DateString;
        public DateTime CreatedDate;

        public DocumentPresModel(string rawText, string[] categoryPhrases, string fileName, string filePath, string createdDate)
        {
            RawText = rawText;
            CategoryPhrases = categoryPhrases;
            FileName = fileName;
            FilePath = filePath;
            DateString = createdDate;
            CreatedDate = ParseDate(createdDate);
        }

        private DateTime ParseDate(string dateString)
        {
            DateTime date = DateTime.MinValue;

            DateTime.TryParse(dateString, out date);

            return date;
        }
    }
}