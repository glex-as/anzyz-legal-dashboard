﻿using Glex.IO.Serializable;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Glex.Core
{
    public class ConceptPresModel
    {
        public Guid ContentId;
        public string Name;
        public string Description;
        public string InitialSearchWord;
        public string[] SearchTerms;
        public string[] IgnoreTerms;

        public List<string> FilePaths;

        public ConceptPresModel(Guid contentId, string name, string description, string initialSearchWord, string[] searchTerms, string[] ignoreTerms, string[] filePaths)
        {
            ContentId = contentId;
            Name = name;
            Description = description;
            InitialSearchWord = initialSearchWord;
            SearchTerms = searchTerms;
            IgnoreTerms = ignoreTerms;

            FilePaths = filePaths != null ? filePaths.ToList() : new List<string>();
        }

        public ConceptPresModel(SerializableConcept concept)
        {
            ContentId = concept.ContentId;
            Name = concept.Name;
            Description = concept.Description;
            InitialSearchWord = concept.InitialSearchWord;
            SearchTerms = concept.SearchTerms;
            IgnoreTerms = concept.IgnoreTerms;

            FilePaths = concept.FilePaths != null ? concept.FilePaths.ToList() : new List<string>();
        }

        public void AddFile(string filePath)
        {
            if(!FilePaths.Contains(filePath))
            {
                FilePaths.Add(filePath);
            }
        }

        public void RemoveFile(string filePath)
        {
            FilePaths.Remove(filePath);
        }
    }
}
