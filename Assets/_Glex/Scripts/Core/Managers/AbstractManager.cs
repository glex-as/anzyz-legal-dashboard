﻿using System.Linq;
using Glex.Pres.Utils;
using UnityEngine;

namespace Glex.Core.Managers
{
    public abstract class Singleton<TSingleton>
        where TSingleton : new()
    {
        public bool IsInitialized { get; internal set; }
        private static TSingleton _instance;
        public static TSingleton Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new TSingleton();
                }
                return _instance;
            }
        }
    }

    public abstract class AbstractManager : MonoBehaviour
    {
        protected static T GetInstance<T>(ref T instance)
            where T : AbstractManager
        {
            if (instance) return instance;

            instance = FindObjectOfType<T>();
            if (instance) return instance;

            GameObject managers = GameObject.Find("Managers");
            if (managers != null) // NOTE: "Managers" can be null when exiting application and the game objects is already destroyed.
            {
                Transform root = managers.transform;
                string goName = typeof(T).ToString().Split('.').LastOrDefault();
                GameObject instanceObj = root.AddChildObject(goName);
                instance = instanceObj.AddComponent<T>();
                instance.Init();
            }

            return instance;
        }

        protected abstract void Init();
    }
}