﻿using BestHTTP;
using Glex.IO;
using Glex.IO.Serializable;
using Glex.Pres.Ui;
using Goontech.Anzyz_API_Client.Scripts;
using Goontech.Anzyz_API_Client.Scripts.Models.Search;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

namespace Glex.Core.Managers
{
    public class MainManager : AbstractManager
    {
        public AdminCanvas AdminCanvas;
        public SearchCanvas SearchCanvas;
        public AnzyzApiClient ApiClient;

        private string _knowledgeBaseName;
        private bool _standardStrictValue = false;

        public string EnronDataBaseFolder;

        public int MaxPhrases { get; private set; }
        public int PageSize { get; private set; }
        public int Rand { get; private set; }


        public int NumberOfPages;
        public int PaginationSize = 20;
        public Dictionary<string, Dictionary<string, object>> MongoDbQuery => GetMongoDbQuery();

        private List<ConceptPresModel> _currentConcepts;
        private List<DocumentPresModel> _currentDocumentsAll;
        public List<RelatedPhrasePresModel> CurrentRelatedPhrasesList;
        public string CurrentSearchTerm;

        public ConceptPresModel ActiveConcept;

        private static MainManager _instance;
        public static MainManager Instance => GetInstance(ref _instance);

        public void Start()
        {
            Init();
        }

        protected override void Init()
        {
            _currentConcepts = ConceptIo.LoadConcepts();
            ActiveConcept = null;
            CurrentRelatedPhrasesList = new List<RelatedPhrasePresModel>();
            _currentDocumentsAll = new List<DocumentPresModel>();
            CurrentSearchTerm = string.Empty;

            var settings = SettingsIo.LoadSettings();

            SetEnronMailBaseFolder(settings.EnronBasePath);
            SetPageSize(settings.PageSize);
            SetMaxPhrases(settings.MaxPhrases);
            SetRandSize(settings.Rand);

            SearchCanvas.Init();
            AdminCanvas.Init();

            AdminCanvas.gameObject.SetActive(true);

            _knowledgeBaseName = AnzyzConstants.KnowledgeBaseName;

            ApiClient = new AnzyzApiClient(AnzyzConstants.AuthUrl, AnzyzConstants.BaseUrl);

            ApiClient.OnLoginSuccess += InitializeAfterLogin;
            ApiClient.OnLoginError += ThrowLoginError;
        }

        #region Auth

        public void Login(string userName, string password)
        {
            string clientId = AnzyzConstants.ClientId;
            string clientSecret = AnzyzConstants.ClientSecret;
            string audience = AnzyzConstants.Audience;
            string grantType = AnzyzConstants.GrantType;

            ApiClient.Login(clientId, clientSecret, audience, grantType);
        }

        private void InitializeAfterLogin()
        {
            AdminCanvas.LoginPage.gameObject.SetActive(false);
            AdminCanvas.ProjectsPage.gameObject.SetActive(true);

            Debug.Log("Login successful");
        }

        private void ThrowLoginError(string message)
        {
            Debug.LogError($"Login failed: {message}");
        }

        #endregion

        #region Terms

        public void StartSearch(string[] targetPhrases, string[] ignorePhrases, int maxPhrases, bool strict)
        {
            TopicRequestModel topicModel = new TopicRequestModel(_knowledgeBaseName, targetPhrases, ignorePhrases, maxPhrases, strict);

            ApiClient.Topic(topicModel, (res, req) => GotTopicResponse(res, req, topicModel.ti));

            SearchCanvas.Terms.ToggleLoading(true);
        }

        private void GotTopicResponse(HTTPRequest req, HTTPResponse res, object[] ti)
        {
            if (res.IsSuccess)
            {
                var responseModel = JsonConvert.DeserializeObject<TopicResponseModel>(res.DataAsText);

                for (var i = 0; i < responseModel.phrase.Length; i++)
                {
                    var phrase = responseModel.phrase[i];
                    var relatedness = responseModel.relatedness[i];
                    var count = responseModel.count[i];

                    RelatedPhrasePresModel relatedPhrase = new RelatedPhrasePresModel(phrase, relatedness, count);
                    CurrentRelatedPhrasesList.Add(relatedPhrase);
                }

                // Get Documents
                GetDocuments(new List<object[]> { ti });
            }
            else
            {
                Debug.LogError($"Search failed with message: {res.Message}");

                SearchCanvas.Terms.ToggleLoading(false);
            }
        }

        public void GetDocuments(List<object[]> ti)
        {
            QueryModel model = new QueryModel(PageSize, MongoDbQuery, _knowledgeBaseName, ti);
            ApiClient.Categorize(model, (res, req) => GotDocuments(res, req, ti));
        }

        private void GotDocuments(HTTPRequest req, HTTPResponse res, List<object[]> ti)
        {
            if (res != null)
            {
                SearchCanvas.Terms.ToggleLoading(false);

                if (res.IsSuccess)
                {
                    string documentResponseText = res.DataAsText;

                    Dictionary<string, object> documentInfoDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(documentResponseText);

                    object obj;
                    documentInfoDict.TryGetValue("document_array", out obj);

                    if (obj != null)
                    {
                        var objAsString = obj.ToString();
                        var documentResponseModel = JsonConvert.DeserializeObject<Document[]>(objAsString);

                        if (documentResponseModel.Any())
                        {
                            int uniqueHits = documentResponseModel
                                .Select(x => x.file_name)
                                .Distinct()
                                .Count();

                            _currentDocumentsAll = documentResponseModel
                                .Select(x => new DocumentPresModel(x.content, x.category_phrases, x.file_name, x.file_path, x.creation_time))
                                .Where(MatchesSearchWord)
                                .ToList();

                            NumberOfPages = _currentDocumentsAll.Count / PaginationSize;

                            if (NumberOfPages < 1)
                            {
                                NumberOfPages = 1;
                            }

                            SearchCanvas.Terms.SetPagination(NumberOfPages);
                            SearchCanvas.Terms.FillDocumentResultList(GetPaginatedResults(1));
                            SearchCanvas.Terms.SetResultsText(_currentDocumentsAll.Count);
                        }
                        else
                        {
                            SearchCanvas.Terms.SetErrorText("Got zero results");
                        }
                    }
                }
                else
                {
                    Debug.Log($"Get Documents failed with message: {res.Message}");
                }
            }
            else
            {
                Debug.Log($"Get Documents returned null, retrying...");

                GetDocuments(ti);
            }
        }

        private bool MatchesSearchWord(DocumentPresModel doc)
        {
            if (CurrentSearchTerm.Equals(string.Empty)) return true;

            if (doc.RawText.ToLower().Contains(CurrentSearchTerm.ToLower())) return true;

            if (doc.CategoryPhrases.Any(x => x.ToLower().Equals(CurrentSearchTerm.ToLower()))) return true;

            return false;
        }

        public List<DocumentPresModel> GetPaginatedResults(int fromPage)
        {
            if (_currentDocumentsAll.Count > PaginationSize)
            {
                var tempArray = new DocumentPresModel[_currentDocumentsAll.Count];
                    
                _currentDocumentsAll.CopyTo(tempArray);

                int startIndex = 0;

                if (fromPage == 0 || fromPage == 1)
                {
                    startIndex = 0;
                }
                else
                {
                    startIndex = (fromPage - 1) * PaginationSize;

                    //startIndex += 1;
                }

                var tempList = new List<DocumentPresModel>();
                tempList.AddRange(tempArray);

                tempList.RemoveRange(0, startIndex);

                var returnList = tempList.Take(PaginationSize).ToList();

                return returnList;
            }
            else
            {
                return _currentDocumentsAll;
            }
        }

        public void SetPageSize(int size)
        {
            PageSize = size;
            SaveSettings();
        }

        public void SetMaxPhrases(int size)
        {
            MaxPhrases = size;
            SaveSettings();
        }

        public void SetRandSize(int size)
        {
            Rand = size;
            SaveSettings();
        }

        #endregion

        #region Concepts

        public void StartConceptSearch(string term)
        {
            string[] targetPhrases = {term};
            string[] ignorePhrases = new string[0];
            var maxPhrases = MaxPhrases;
            var strict = _standardStrictValue;

            TopicRequestModel topicModel = new TopicRequestModel(_knowledgeBaseName, targetPhrases, ignorePhrases, maxPhrases, strict);

            ApiClient.Topic(topicModel, (res, req) => GotTermSuggestions(res, req, topicModel.ti));
        }

        private void GotTermSuggestions(HTTPRequest req, HTTPResponse res, object[] ti)
        {
            if (res.IsSuccess)
            {
                List<RelatedPhrasePresModel> phrases = new List<RelatedPhrasePresModel>();

                var responseModel = JsonConvert.DeserializeObject<TopicResponseModel>(res.DataAsText);

                for (var i = 0; i < responseModel.phrase.Length; i++)
                {
                    var phrase = responseModel.phrase[i];
                    var relatedness = responseModel.relatedness[i];
                    var count = responseModel.count[i];

                    RelatedPhrasePresModel relatedPhrase = new RelatedPhrasePresModel(phrase, relatedness, count);
                    phrases.Add(relatedPhrase);
                }

                SearchCanvas.Concepts.ToggleAndSetConceptValues(phrases);
            }
            else
            {
                Debug.LogError($"Search failed with message: {res.Message}");

                SearchCanvas.Terms.ToggleLoading(false);
            }
        }

        public void SaveOrReplaceConcept(ConceptPresModel concept)
        {
            SaveOrReplaceConcept(
                concept.ContentId,
                concept.Name, 
                concept.Description, 
                concept.InitialSearchWord, 
                concept.SearchTerms,
                concept.IgnoreTerms, 
                concept.FilePaths.ToArray());
        }

        public void SaveOrReplaceConcept(string conceptName, string conceptDescription, string initialSearchWord, string[] terms, string[] ignoreTerms)
        {
            SaveOrReplaceConcept(Guid.NewGuid(), conceptName, conceptDescription, initialSearchWord, terms, ignoreTerms, new string[0]);
        }

        public void SaveOrReplaceConcept(Guid conceptId, string conceptName, string conceptDescription, string initialSearchWord, string[] terms, string[] ignoreTerms, string[] filePaths)
        {
            ConceptPresModel newConcept = new ConceptPresModel(conceptId, conceptName, conceptDescription, initialSearchWord, terms, ignoreTerms, filePaths);

            if (!_currentConcepts.Any(x => x.ContentId.Equals(newConcept.ContentId)))
            {
                _currentConcepts.Add(newConcept);
                ConceptIo.SaveConcept(newConcept);
            }
            else
            {
                var oldValue = _currentConcepts.FirstOrDefault(x => x.ContentId.Equals(conceptId));
                int index = _currentConcepts.IndexOf(oldValue);
                if (index != -1)
                {
                    _currentConcepts[index] = newConcept;
                }
                ConceptIo.SaveConcept(newConcept);
            }
        }

        public void DeleteConcept(Guid conceptId)
        {
            if (_currentConcepts.Any(x => x.ContentId.Equals(conceptId)))
            {
                ConceptIo.DeleteConcept(conceptId);

                _currentConcepts = ConceptIo.LoadConcepts();
            }

            SearchCanvas.Concepts.RefreshConceptsList();
        }

        public List<ConceptPresModel> GetConcepts()
        {
            return _currentConcepts;
        }

        public void OpenSaveToFolderDialog(DocumentPresModel document)
        {
            SearchCanvas.OpenAddToFolderDialog(document);
        }

        public void AddDocumentToConcept(DocumentPresModel document, ConceptPresModel concept)
        {
            concept.AddFile(document.FilePath);
            SaveOrReplaceConcept(concept);

            //if (ActiveConcept != null)
            //{
            //    ActiveConcept.AddFile(document.FilePath);

            //    SaveOrReplaceConcept(ActiveConcept);
            //}
        }

        #endregion

        #region Timeline

        public void StartTimelineSearch(string[] targetPhrases, string[] ignorePhrases, int maxPhrases, bool strict)
        {
            TopicRequestModel topicModel = new TopicRequestModel(_knowledgeBaseName, targetPhrases, ignorePhrases, maxPhrases, strict);

            ApiClient.Topic(topicModel, (res, req) => GotTimelineTopicResponse(res, req, topicModel.ti));

            SearchCanvas.Timeline.ToggleLoading(true);
        }

        private void GotTimelineTopicResponse(HTTPRequest req, HTTPResponse res, object[] ti)
        {
            if (res.IsSuccess)
            {
                var responseModel = JsonConvert.DeserializeObject<TopicResponseModel>(res.DataAsText);

                //for (var i = 0; i < responseModel.phrase.Length; i++)
                //{
                //    var phrase = responseModel.phrase[i];
                //    var relatedness = responseModel.relatedness[i];
                //    var count = responseModel.count[i];

                //    RelatedPhrasePresModel relatedPhrase = new RelatedPhrasePresModel(phrase, relatedness, count);
                //    //CurrentRelatedPhrasesList.Add(relatedPhrase);
                //}

                // Get Emails
                GetTimelineEmails(new List<object[]> { ti });
            }
            else
            {
                Debug.LogError($"Search failed with message: {res.Message}");

                SearchCanvas.Timeline.ToggleLoading(false);
            }
        }

        public void GetTimelineEmails(List<object[]> ti)
        {
            QueryModel model = new QueryModel(PageSize, MongoDbQuery, _knowledgeBaseName, ti);
            ApiClient.Categorize(model, (res, req) => GotEmails(res, req, ti));
        }

        private void GotEmails(HTTPRequest req, HTTPResponse res, List<object[]> ti)
        {
            if (res != null)
            {
                SearchCanvas.Timeline.ToggleLoading(false);

                if (res.IsSuccess)
                {
                    string documentResponseText = res.DataAsText;

                    Dictionary<string, object> documentInfoDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(documentResponseText);

                    object obj;
                    documentInfoDict.TryGetValue("document_array", out obj);

                    if (obj != null)
                    {
                        var objAsString = obj.ToString();
                        var documentResponseModel = JsonConvert.DeserializeObject<Document[]>(objAsString);

                        if (documentResponseModel.Any())
                        {
                            _currentDocumentsAll = documentResponseModel
                                .Select(x => new DocumentPresModel(x.content, x.category_phrases, x.file_name, x.file_path, x.creation_time))
                                .ToList();

                            SearchCanvas.Timeline.FillDocumentResultList(_currentDocumentsAll);
                        }
                        else
                        {
                            //SearchCanvas.Terms.SetErrorText("Got zero results");
                        }
                    }
                }
                else
                {
                    Debug.Log($"Get Documents failed with message: {res.Message}");
                }
            }
            else
            {
                Debug.Log($"Get Documents returned null, retrying...");

                GetTimelineEmails(ti);
            }
        }

        #endregion

        #region Documents

        public EmailPresModel GetDocumentFromFilePath(string filePath)
        {
            var email = EmailIo.LoadEmailWithFilePath(filePath);

            return email;
        }

        #endregion

        #region Projects

        public void OpenProjectsPage()
        {
            SearchCanvas.gameObject.SetActive(false);
            AdminCanvas.gameObject.SetActive(true);
            AdminCanvas.ProjectsPage.gameObject.SetActive(true);
            AdminCanvas.LoginPage.gameObject.SetActive(false);
        }

        public void OpenProject(SerializableProject project)
        {
            AdminCanvas.gameObject.SetActive(false);
            SearchCanvas.gameObject.SetActive(true);
            SearchCanvas.BreadCrumbsBar.SetCaseNameText(project.Name);
            SearchCanvas.SideMenuBar.SetProjectName(project.Name);
        }

        #endregion

        #region Utils

        private Dictionary<string, Dictionary<string, object>> GetMongoDbQuery()
        {
            Dictionary<string, Dictionary<string, object>> dict = new Dictionary<string, Dictionary<string, object>>();

            var randDict = new Dictionary<string, object>();
            randDict.Add("$lte", Rand);

            dict.Add("_rand", randDict);

            return dict;
        }

        public void SetEnronMailBaseFolder(string path)
        {
#if UNITY_STANDALONE_WIN
            if (!path.Equals(string.Empty) && !path.EndsWith("\\"))
            {
                path += @"\";
            }
#endif
            EnronDataBaseFolder = path;

            SaveSettings();

            Debug.Log($"Enron path set to: {path}");
        }

        public void SaveSettings()
        {
            SettingsIo.SaveSettings(EnronDataBaseFolder, MaxPhrases, PageSize, Rand);
        }

        #endregion
    }
}