﻿namespace Glex.Core
{
    public class RelatedPhrasePresModel
    {
        public string Phrase;
        public string Relatedness;
        public float Count;

        public RelatedPhrasePresModel(string phrase, string relatedness, float count)
        {
            Phrase = phrase;
            Relatedness = relatedness;
            Count = count;
        }
    }
}