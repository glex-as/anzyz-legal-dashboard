﻿using System;
using System.Text.RegularExpressions;

namespace Glex.Core
{
    public class EmailPresModel
    {
        public string MessageId;
        public string Date;
        public string FromEmail;
        public string ToEmails;
        public string Subject;
        public string MimeVersion;
        public string ContentType;
        public string ContentTransferEncoding;
        public string XFrom;
        public string XTo;
        public string XBcc;
        public string XFolder;
        public string XOrigin;
        public string XFileName;
        public string MainText;

        public EmailPresModel(string messageId, string date, string fromEmail, string emails, string subject, string mimeVersion, string contentType, string contentTransferEncoding, string xFrom, string xTo, string xBcc, string xFolder, string xOrigin, string xFileName, string mainText)
        {
            MessageId = messageId;
            Date = date;
            FromEmail = fromEmail;
            ToEmails = emails;
            Subject = subject;
            MimeVersion = mimeVersion;
            ContentType = contentType;
            ContentTransferEncoding = contentTransferEncoding;
            XFrom = xFrom;
            XTo = xTo;
            XBcc = xBcc;
            XFolder = xFolder;
            XOrigin = xOrigin;
            XFileName = xFileName;
            MainText = mainText;
        }

        public EmailPresModel(string emailText)
        {
            string[] lines = Regex.Split(emailText, Environment.NewLine);

            foreach (string s in lines)
            {
                if (s.StartsWith("Message-ID:"))
                {
                    MessageId = s.Replace("Message-ID:", "");
                }
                else if (s.StartsWith("Date:"))
                {
                    Date = s.Replace("Date:", "");
                }
                else if (s.StartsWith("From:"))
                {
                    FromEmail = s.Replace("From:", "");
                }
                else if (s.StartsWith("To:"))
                {
                    ToEmails = s.Replace("To:", "");
                }
                else if (s.StartsWith("Subject:"))
                {
                    Subject = s.Replace("Subject:", "");
                }
                else if (s.StartsWith("Content-Type:"))
                {
                    ContentType = s.Replace("Content-Type:", "");
                }
                else if (s.StartsWith("Content-Transfer-Encoding:"))
                {
                    ContentTransferEncoding = s.Replace("Content-Transfer-Encoding:", "");
                }
                else if (s.StartsWith("X-From:"))
                {
                    XFrom = s.Replace("X-From:", "");
                }
                else if (s.StartsWith("X-To:"))
                {
                    XTo = s.Replace("X-To:", "");
                }
                else if (s.StartsWith("X-cc:"))
                {
                    XBcc = s.Replace("X-cc:", "");
                }
                else if (s.StartsWith("X-Folder:"))
                {
                    XFolder = s.Replace("X-Folder:", "");
                }
                else if (s.StartsWith("X-Origin:"))
                {
                    XOrigin = s.Replace("X-Origin:", "");
                }
                else if (s.StartsWith("X-FileName:"))
                {
                    XFileName = s.Replace("X-FileName:", "");
                }
                else
                {
                    MainText = s;
                }
            }
        }
    }
}