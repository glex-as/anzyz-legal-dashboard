using System.Collections.Generic;

namespace UnityEngine.UI
{
    [AddComponentMenu("UI/Effects/SoftShadow", 32)]
    public class SoftShadow : BaseMeshEffect
    {
        [SerializeField]
        private Color m_EffectColor = new Color(0.0f, 0.0f, 0.0f, 0.1f);

        [SerializeField]
        private Vector2 m_EffectDistance = new Vector2(0, 0);

        [SerializeField]
        private Vector2 m_EffectPosition = new Vector2(2, -2);

        [SerializeField]
        private int m_EffectSteps = 4;

        #if UNITY_EDITOR
        protected override void OnValidate()
        {
            this.effectDistance = this.m_EffectDistance;
            this.effectPosition = this.m_EffectPosition;
            base.OnValidate();
        }
        #endif

        /// <summary>
        ///   <para>Color for the effect.</para>
        /// </summary>
        public Color effectColor
        {
            get
            {
                return this.m_EffectColor;
            }
            set
            {
                this.m_EffectColor = value;
                if (!((Object)this.graphic != (Object)null))
                    return;
                this.graphic.SetVerticesDirty();
            }
        }

        /// <summary>
        ///   <para>How far is the shadow from the graphic.</para>
        /// </summary>
        public Vector2 effectDistance
        {
            get
            {
                return this.m_EffectDistance;
            }
            set
            {
                if (this.m_EffectDistance == value)
                    return;
                this.m_EffectDistance = value;
                if (!((Object)this.graphic != (Object)null))
                    return;
                this.graphic.SetVerticesDirty();
            }
        }

        /// <summary>
        ///   <para>How far is the shadow from the graphic.</para>
        /// </summary>
        public Vector2 effectPosition
        {
            get
            {
                return this.m_EffectPosition;
            }
            set
            {
                if (this.m_EffectPosition == value)
                    return;
                this.m_EffectDistance = value;
                if (!((Object)this.graphic != (Object)null))
                    return;
                this.graphic.SetVerticesDirty();
            }
        }

        public int effectSteps
        {
            get { return this.m_EffectSteps; }
            set
            {
                if (this.m_EffectSteps == value)
                    return;
                this.m_EffectSteps = Mathf.Max(Mathf.Min(10, value), -10);
                if (!((Object)this.graphic != (Object)null))
                    return;
                this.graphic.SetVerticesDirty();
            }
        }

        protected void ApplyShadowZeroAlloc(List<UIVertex> verts, Color32 color, int start, int end, Vector2 effectDistance, Vector2 effectPosition, int effectSteps, int currentStep)
        {
            int num = verts.Count + end - start;
            if (verts.Capacity < num)
                verts.Capacity = num;
            for (int index = start; index < end; ++index)
            {
                UIVertex vert = verts[index];
                verts.Add(vert);
                Vector2 position = vert.position;

                // Scale shadow
                position.x *= 1 + (effectDistance.x / GetComponent<RectTransform>().rect.width) * (float)(currentStep / effectSteps);
                position.y *= 1 + (effectDistance.y / GetComponent<RectTransform>().rect.height) * (float)(currentStep / effectSteps);

                // Move shadow
                position += (effectPosition / (float)currentStep);

                vert.position = position;
                Color32 color32 = new Color32(color.r, color.g, color.b, (byte)(color.a / effectSteps));
                vert.color = color32;
                verts[index] = vert;
            }
        }

        protected void ApplyShadow(List<UIVertex> verts, Color32 color, int start, int end, Vector2 effectDistance, Vector2 effectPosition, int effectSteps, int currentStep)
        {
            this.ApplyShadowZeroAlloc(verts, color, start, end, effectDistance, effectPosition, effectSteps, currentStep);
        }

        public override void ModifyMesh(VertexHelper vh)
        {
            if (!this.IsActive())
                return;
            List<UIVertex> uiVertexList = ListPool<UIVertex>.Get();
            vh.GetUIVertexStream(uiVertexList);

            for (int i = 1; i <= effectSteps; i++)
            {
                this.ApplyShadow(uiVertexList, (Color32)this.effectColor, 0, uiVertexList.Count, this.effectDistance, this.effectPosition, this.effectSteps, i);
            }

            vh.Clear();
            vh.AddUIVertexTriangleStream(uiVertexList);
            ListPool<UIVertex>.Release(uiVertexList);
        }
    }
}
