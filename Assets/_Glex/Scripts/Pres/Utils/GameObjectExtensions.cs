﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Glex.Pres.Utils
{
    public static class GameObjectExtensions
    {
        // GameObject

        public static GameObject AddChildObject(this GameObject parent, string childName)
        {
            return AddChildObject(parent.transform, childName);
        }

        public static GameObject AddChildObject(this Component parent, string childName)
        {
            return AddChildObject(parent.transform, childName);
        }

        private static GameObject AddChildObject(Transform parentTransform, string childName)
        {
            GameObject child = new GameObject(childName);
            child.transform.SetParent(parentTransform, false);
            return child;
        }

        public static void SetLayerIncludingChildren(this GameObject gameObject, int layer)
        {
            gameObject.layer = layer;

            foreach (Transform child in gameObject.transform)
            {
                child.gameObject.SetLayerIncludingChildren(layer);
            }
        }

        /// <summary> Adds child object with scale = Vector3.one and position = Vector.zero </summary>
        public static GameObject AddNormalizedChildObject(this GameObject parent, string childName)
        {
            GameObject child = parent.AddChildObject(childName);
            return child.transform.Normalize().gameObject;
        }

        public static bool IsSelected(this GameObject gameObject)
        {
            return EventSystem.current.currentSelectedGameObject == gameObject;
        }

        // Transform

        /// <summary> Sets transform position to (0,0,0), eulerAngles to (0,0,0), scale to (1,1,1) </summary>
        public static Transform Normalize(this Transform transform)
        {
            transform.position = Vector3.zero;
            transform.eulerAngles = Vector3.zero;
            transform.localScale = Vector3.one;

            return transform;
        }

        /// <summary> Sets transform.localPosition to (x, 0, z) </summary>
        public static Transform ResetLocalPositionY(this Transform transform)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);
            return transform;
        }

        /// <summary> Sets transform.localPosition to (x, 0, z) </summary>
        public static Transform ResetWorldPositionY(this Transform transform)
        {
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
            return transform;
        }

        public static IEnumerable<Transform> Children(this Transform transform)
        {
            foreach (Transform child in transform)
            {
                yield return child;
            }
        }

        // LineRenderer

        public static Vector2[] Points(this LineRenderer lineRenderer)
        {
            return Enumerable
                .Range(0, lineRenderer.positionCount)
                .Select(lineRenderer.PointAtIndex)
                .ToArray();
        }

        private static Vector2 PointAtIndex(this LineRenderer lineRenderer, int index)
        {
            Vector3 position = lineRenderer.GetPosition(index);
            return new Vector2(position.x, position.z);
        }

        public static void SetColor(this LineRenderer line, Color color)
        {
            line.startColor = color;
            line.endColor = color;
        }

        public static void SetWidth(this LineRenderer line, float width)
        {
            line.startWidth = width;
            line.endWidth = width;
        }

        /// <summary> Get or add component </summary>
        public static TComponent EnsureComponent<TComponent>(this Component component)
            where TComponent : Component => EnsureComponent<TComponent>(component.gameObject);
        /// <summary> Get or add component </summary>
        public static TComponent EnsureComponent<TComponent>(this GameObject gameObject) 
            where TComponent : Component
        {
            TComponent comp = gameObject.GetComponent<TComponent>();
            return comp != null ? comp : gameObject.AddComponent<TComponent>();
        }

        /// <summary> Destroy component if any </summary>
        public static void TryRemoveComponent<TComponent>(this Component component)
            where TComponent : Component => TryRemoveComponent<TComponent>(component?.gameObject);
        /// <summary> Destroy component if any </summary>
        public static void TryRemoveComponent<TComponent>(this GameObject gameObject) 
            where TComponent : Component
        {
            TComponent component = gameObject?.GetComponent<TComponent>();
            if (component) Object.Destroy(component);
        }

        public static void SetAspectRatio(this Image image)
        {
            if (!image.sprite) return;

            image.preserveAspect = true;
            AspectRatioFitter arf = image.EnsureComponent<AspectRatioFitter>();

            arf.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
            arf.aspectRatio = image.sprite.rect.width / image.sprite.rect.height;
            if (float.IsNaN(arf.aspectRatio))
            {
                arf.aspectRatio = 1;
            }
        }

        // Others

        /// <summary> Create new sprite element of this texutre </summary>
        public static Sprite GetSprite(this Texture2D texture)
        {
            if (texture == null)
                throw new MissingReferenceException($"GetSprite: Texture2D is null");

            Rect rect = new Rect(0, 0, texture?.width ?? 2, texture?.height ?? 2);
            return Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f), 100);
        }

        /// <summary> Scales whole element relative to source image size. Size is based on width </summary>
        public static void SetAbsoluteSize(this SpriteRenderer spriteRenderer, float size)
        {
            Vector2 spriteSize = spriteRenderer.sprite.textureRect.size;
            float newSpriteSize = size / spriteSize.x;
            spriteRenderer.transform.localScale = Vector3.one * newSpriteSize;
        }

        public static void AddPoint(this LineRenderer lineRenderer, Vector3 point)
        {
            lineRenderer.positionCount++;
            lineRenderer.SetPosition(lineRenderer.positionCount - 1, point);
        }

        // EventTrigger

        public static EventTrigger SetTriggerEvent(this EventTrigger trigger, EventTriggerType triggerType, Action callAction)
        {
            EventTrigger.Entry entry = new EventTrigger.Entry {eventID = triggerType};
            entry.callback.AddListener(_ => callAction());
            trigger.triggers.Add(entry);
            return trigger;
        }

        public static EventTrigger RemoveAllTriggerEvents(this EventTrigger trigger)
        {
            foreach (EventTrigger.Entry entry in trigger.triggers)
            {
                entry.callback.RemoveAllListeners();
            }

            trigger.triggers.Clear();
            return trigger;
        }

        public static EventTrigger SetHoverEvents(this EventTrigger trigger, Action<bool> onHoveredChanged) =>
            SetHoverEvents(trigger, () => onHoveredChanged(true), () => onHoveredChanged(false));
        public static EventTrigger SetHoverEvents(this EventTrigger trigger, Action onPointerEnter, Action onPointerExit)
        {
            trigger.SetTriggerEvent(EventTriggerType.PointerEnter, onPointerEnter);
            trigger.SetTriggerEvent(EventTriggerType.PointerExit, onPointerExit);
            return trigger;
        }
        
        public static EventTrigger SetPointerEvents(this EventTrigger trigger, Action<bool> onDownChanged) =>
            SetHoverEvents(trigger, () => onDownChanged(true), () => onDownChanged(false));
        public static EventTrigger SetPointerEvents(this EventTrigger trigger, Action onPointerDown, Action onPointerUp)
        {
            trigger.SetTriggerEvent(EventTriggerType.PointerDown, onPointerDown);
            trigger.SetTriggerEvent(EventTriggerType.PointerUp, onPointerUp);
            return trigger;
        }

        public static EventTrigger SetDragEvents(this EventTrigger trigger, Action<bool> onDragChanged) =>
            SetHoverEvents(trigger, () => onDragChanged(true), () => onDragChanged(false));
        public static EventTrigger SetDragEvents(this EventTrigger trigger, Action onGrab, Action onDrop)
        {
            trigger.SetTriggerEvent(EventTriggerType.BeginDrag, onGrab);
            trigger.SetTriggerEvent(EventTriggerType.EndDrag, onDrop);
            return trigger;
        }

        public static Vector2 GetCalculatedSize(this HorizontalOrVerticalLayoutGroup layout)
        {
            Vector2 items = Vector2.zero;
            foreach (RectTransform child in layout.GetComponentsInChildren<RectTransform>())
                items += child.sizeDelta;
            
            float spacing = layout.spacing * (layout.transform.childCount - 1);

            Vector2 paddingAndSpacing = layout is VerticalLayoutGroup
                ? Vector2.up * (layout.padding.top + layout.padding.bottom + spacing)
                : Vector2.right * (layout.padding.right + layout.padding.left + spacing);

            return items + paddingAndSpacing;
        }
    }
}