﻿using Glex.Core.Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui
{
    public class LeftSideMenuBar : MonoBehaviour
    {
        [SerializeField] private TMP_Text _projectNameText;
        [SerializeField] private Button _projectsButton;

        [Header("Pages")]
        [SerializeField] private Button _termsButton;
        [SerializeField] private Button _conceptsButton;
        [SerializeField] private Button _timelineButton;
        [SerializeField] private Button _documentsButton;
        [Space]
        [SerializeField] private Button _downloadDocumentsButton;
        [SerializeField] private Button _openSettingsButton;

        private SearchCanvas _searchCanvas;

        public void Init(SearchCanvas canvas)
        {
            _searchCanvas = canvas;

            _termsButton.onClick.AddListener(_searchCanvas.OpenTermsPage); // Todo: Consider changing to event pattern
            _conceptsButton.onClick.AddListener(_searchCanvas.OpenConceptsPage); // Todo: Consider changing to event pattern
            _timelineButton.onClick.AddListener(_searchCanvas.OpenTimelinePage); // Todo: Consider changing to event pattern
            _documentsButton.onClick.AddListener(_searchCanvas.OpenDocumentsPage); // Todo: Consider changing to event pattern

            _projectsButton.onClick.AddListener(MainManager.Instance.OpenProjectsPage);

            _openSettingsButton.onClick.AddListener(_searchCanvas.OpenSettingsDialog);
        }

        public void SetProjectName(string projectName)
        {
            _projectNameText.text = projectName.ToUpper();
        }
    }
}