﻿using Glex.Core.Managers;
using Glex.Pres.Ui.Concepts;
using System;
using Glex.IO.Serializable;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui
{
    public class AdminCanvas : MonoBehaviour
    {
        public Button LoginButton;
        public RectTransform LoginPage;
        public RectTransform ProjectsPage;

        [SerializeField] private GridLayoutGroup _projectsGrid;
        [SerializeField] private ProjectPrefab _projectPrefab;

        public void Init()
        {
            LoginButton.onClick.AddListener(() => MainManager.Instance.Login("", ""));

            ProjectsPage.gameObject.SetActive(false);
            LoginPage.gameObject.SetActive(true);

            AddProjects();
        }

        private void AddProjects()
        {
            SerializableProject project = new SerializableProject(
                Guid.NewGuid(),
                "Enron", 
                "The Enron scandal was an accounting scandal involving Enron Corporation, an American energy company based in Houston, Texas. Upon being publicized in October 2001, the company declared bankruptcy and its accounting firm, Arthur Andersen – then one of the five largest audit and accountancy partnerships in the world – was effectively dissolved.",
                new [] { "Jorgen Engen Napstad", "Ole-Johan Meyer Melin"});

            var prefab = Instantiate(_projectPrefab, _projectsGrid.transform);
            prefab.Init(project);
        }
    }
}