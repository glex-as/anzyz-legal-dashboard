﻿using Glex.Core;
using Glex.Core.Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Terms
{
    public class EmailPreviewPrefab : MonoBehaviour
    {
        [SerializeField] private Button _openButton;
        [SerializeField] private TMP_Text _fromNameText;
        [SerializeField] private TMP_Text _dateText;
        [SerializeField] private TMP_Text _subjectText;
        [SerializeField] private TMP_Text _mainText;

        private DocumentPresModel _doc;
        private EmailPresModel _email;

        public void Init(DocumentPresModel doc, EmailPresModel email)
        {
            _doc = doc;
            _email = email;

            _openButton.onClick.AddListener(OpenInFullScreenPreview);
            _openButton.onClick.AddListener(OpenInFullScreenPreview);

            _fromNameText.text = email.XFrom;
            _dateText.text = email.Date;
            _subjectText.text = email.Subject;
            _mainText.text = email.MainText;
        }

        private void OpenInFullScreenPreview()
        {
            MainManager.Instance.SearchCanvas.OpenDocumentPreview(_doc);
        }
    }
}