﻿using System.IO;
using Glex.Core;
using Glex.Core.Managers;
using Glex.Pres.Ui.Documents;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Terms
{
    public class DocumentPreviewPrefab : DocumentTextHighlightBase
    {
        [SerializeField] private Button _openButton;
        [SerializeField] private Button _addToFolderButton;
        [SerializeField] private Button _downloadButton;
        [SerializeField] private Button _bookmarkButton;
        
        [SerializeField] private Button _fileNameButton;
        [SerializeField] private TMP_Text _fileNameText;

        public void Init(DocumentPresModel doc, EmailPresModel email, string searchWord)
        {
            HighlightText(doc, email, searchWord);
            _fileNameText.text = email.XFileName;

            _openButton.onClick.AddListener(OpenInFullScreenPreview);
            _fileNameButton.onClick.AddListener(OpenInFullScreenPreview);
            _addToFolderButton.onClick.AddListener(AddToFolder);
            _downloadButton.onClick.AddListener(() => Download(doc.FilePath));
            _bookmarkButton.onClick.AddListener(Bookmark);
        }

        private void OpenInFullScreenPreview()
        {
            MainManager.Instance.SearchCanvas.OpenDocumentPreview(Document);
        }

        private void AddToFolder()
        {
            MainManager.Instance.OpenSaveToFolderDialog(Document);
        }

        private void Download(string url)
        {
            // Open document based on url / file path
            //Application.OpenURL(url);

            OpenFile(url);
        }

        private void Bookmark()
        {
            
        }

        public void Cleanup()
        {
            _addToFolderButton.onClick.RemoveAllListeners();
            _downloadButton.onClick.RemoveAllListeners();
            _bookmarkButton.onClick.RemoveAllListeners();
        }

        private void OpenFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return;
            }

            // combine the arguments together
            // it doesn't matter if there is a space after ','
            string argument = "/select, \"" + filePath + "\"";

            System.Diagnostics.Process.Start("explorer.exe", argument);
        }
    }
}
