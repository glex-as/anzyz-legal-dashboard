﻿using Glex.Core;
using Glex.Core.Managers;
using Glex.Pres.Ui.Concepts;
using Michsky.UI.ModernUIPack;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Terms
{
    public class TermsPage : MonoBehaviour
    {
        [SerializeField] private TMP_InputField _searchInputField;
        [SerializeField] private GridLayoutGroup _documentGrid;
        [SerializeField] private DocumentPreviewPrefab _docPreviewPrefab;
        [SerializeField] private TMP_Text _topicResultCountText;
        [SerializeField] private UIManagerProgressBarLoop _progressBar;

        [Header("Concept Suggestions:")] 
        [SerializeField] private GridLayoutGroup _conceptSuggestionGrid;
        [SerializeField] private ConceptSuggestionPrefab _conceptSuggesionPrefab;
        [SerializeField] private Transform _addConceptHeader;
        public AddConceptToSearchInputPrefab AddToSearchInput;

        [Header("Pagination:")]
        [SerializeField] private HorizontalLayoutGroup _paginationLayoutGroup;
        [SerializeField] private PageButtonPrefab _pageButtonPrefab;

        private List<DocumentPreviewPrefab> _documents;
        

        public void Init()
        {
            _documents = new List<DocumentPreviewPrefab>();

            _searchInputField.onEndEdit.AddListener(Search);

            AddToSearchInput.Init();

            ClearDocumentResultList();
            _topicResultCountText.text = "";

            ToggleLoading(false);
        }

        private void Search(string inputText)
        {
            if (!inputText.Equals(string.Empty))
            {
                if (MainManager.Instance.ActiveConcept != null)
                {
                    Search(MainManager.Instance.ActiveConcept.SearchTerms, inputText);

                    MainManager.Instance.SearchCanvas.BreadCrumbsBar.SetSearchTermText(MainManager.Instance.ActiveConcept.Name);
                }
                else
                {
                    var targetPhrases = _searchInputField.text.Split(',');
                    Search(targetPhrases, "");
                    MainManager.Instance.SearchCanvas.BreadCrumbsBar.SetSearchTermText(inputText);
                }
            }
        }

        private void Search(string[] searchTerms, string searchFieldText)
        {
            if (searchTerms.Length > 0)
            {
                MainManager.Instance.CurrentSearchTerm = searchFieldText;

                ClearDocumentResultList();

                var ignorePhrases = new string[0];
                var maxPhrases = MainManager.Instance.MaxPhrases;
                bool strict = false;

                MainManager.Instance.StartSearch(searchTerms, ignorePhrases, maxPhrases, strict);
            }
        }

        private void ClearDocumentResultList()
        {
            if (_documents.Any())
            {
                foreach (var document in _documents)
                {
                    document.Cleanup();

                    Destroy(document.gameObject);
                }
            }

            if (_documentGrid.transform.childCount > 0)
            {
                foreach (Transform child in _documentGrid.transform)
                {
                    Destroy(child.gameObject);
                }
            }

            _documents = new List<DocumentPreviewPrefab>();
        }

        public void SetResultsText(int value)
        {
            int paginationSize = MainManager.Instance.PaginationSize;

            if (paginationSize > value)
            {
                paginationSize = value;
            }

            _topicResultCountText.text = $"{paginationSize} of {value} results";
        }

        public void SetErrorText(string text)
        {
            _topicResultCountText.text = text;
        }

        public void SetPagination(int numberOfPages)
        {
            ClearPaginationButtons();

            if (numberOfPages > 1)
            {
                for (int i = 0; i < numberOfPages; i++)
                {
                    var prefab = Instantiate(_pageButtonPrefab, _paginationLayoutGroup.transform);
                    prefab.Init(i + 1);

                    if (i == 0)
                    {
                        prefab.SetPageButton.isOn = true;
                    }
                }
            }
        }

        private void ClearPaginationButtons()
        {
            if (_paginationLayoutGroup.transform.childCount > 0)
            {
                foreach (Transform child in _paginationLayoutGroup.transform)
                {
                    Destroy(child.gameObject);
                }
            }
        }

        public void FillDocumentResultList(List<DocumentPresModel> documents)
        {
            ClearDocumentResultList();

            foreach (var document in documents)
            {
                var prefab = Instantiate(_docPreviewPrefab, _documentGrid.transform);
                _documents.Add(prefab);

                var email = MainManager.Instance.GetDocumentFromFilePath(document.FilePath);

                prefab.Init(document, email, MainManager.Instance.CurrentSearchTerm);
            }
        }

        public void AddConceptToSearch(ConceptPresModel concept)
        {
            var conceptPrefab = Instantiate(_conceptSuggesionPrefab, _conceptSuggestionGrid.transform);
            conceptPrefab.Init(concept);

            // Todo: Add to search logic
            MainManager.Instance.ActiveConcept = concept;

            Search(concept.SearchTerms, _searchInputField.text);

            MainManager.Instance.SearchCanvas.BreadCrumbsBar.SetSearchTermText(concept.Name);

            _addConceptHeader.transform.SetAsFirstSibling();
            AddToSearchInput.transform.SetAsLastSibling();

            AddToSearchInput.ResetDropdown();
        }

        public void RemoveConceptFromSearch(ConceptPresModel concept)
        {
            MainManager.Instance.ActiveConcept = null;

            // Todo: Remove from search logic
        }
        
        public void ToggleLoading(bool isLoading)
        {
            _progressBar.transform.gameObject.SetActive(isLoading);
        }
    }
}