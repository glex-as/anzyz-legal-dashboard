﻿using Glex.Core;
using Glex.Core.Managers;
using Glex.Pres.Ui.Terms;
using System;
using System.Collections.Generic;
using System.Linq;
using Michsky.UI.ModernUIPack;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui
{
    public class TimelinePage : MonoBehaviour
    {
        [SerializeField] private TMP_InputField _searchInputField;
        [SerializeField] private UIManagerProgressBarLoop _progressBar;
        [SerializeField] private RectTransform _timelineGraphTransform;
        [SerializeField] private RectTransform _timelineListTransform;
        [SerializeField] private VerticalLayoutGroup _resultsList;
        [SerializeField] private EmailPreviewPrefab _emailPrefab;
        [SerializeField] private Transform _datePrefab;

        public void Init()
        {
            ResetPage();
            
            _searchInputField.onEndEdit.AddListener(Search);

            ToggleLoading(false);
        }

        private void Search(string inputText)
        {
            if (!inputText.Equals(string.Empty))
            {
                var targetPhrases = _searchInputField.text.Split(',');
                Search(targetPhrases);
                MainManager.Instance.SearchCanvas.BreadCrumbsBar.SetSearchTermText(inputText);
            }
        }

        private void Search(string[] searchTerms)
        {
            if (searchTerms.Length > 0)
            {
                ClearDocumentResultList();

                var ignorePhrases = new string[0];
                var maxPhrases = MainManager.Instance.MaxPhrases;
                bool strict = false;

                MainManager.Instance.StartTimelineSearch(searchTerms, ignorePhrases, maxPhrases, strict);
            }
        }

        public void FillDocumentResultList(List<DocumentPresModel> documents)
        {
            ClearDocumentResultList();

            Dictionary<DateTime, List<DocumentPresModel>> dateAndDocsDictionary = new Dictionary<DateTime, List<DocumentPresModel>>();

            var dates = documents
                .Select(x => x.CreatedDate)
                .OrderByDescending(x => x.Date)
                .ToList()
                .Distinct();

            foreach (var date in dates)
            {
                var documentsAtDate = documents.Where(x => date.Date.Equals(x.CreatedDate.Date)).ToList();

                dateAndDocsDictionary.Add(date, documentsAtDate);
            }

            foreach (var date in dateAndDocsDictionary.Keys)
            {
                var dateObject = Instantiate(_datePrefab, _resultsList.transform);
                dateObject.GetComponentInChildren<TMP_Text>().text = date.ToString();

                List<DocumentPresModel> docs = new List<DocumentPresModel>();

                dateAndDocsDictionary.TryGetValue(date, out docs);

                foreach (var document in docs)
                {
                    var prefab = Instantiate(_emailPrefab, _resultsList.transform);
                    var email = MainManager.Instance.GetDocumentFromFilePath(document.FilePath);
                    prefab.Init(document, email);
                }
            }

            _timelineGraphTransform.gameObject.SetActive(true);
            _timelineListTransform.gameObject.SetActive(true);
        }

        private void ClearDocumentResultList()
        {
            if (_resultsList.transform.childCount > 0)
            {
                foreach (Transform child in _resultsList.transform)
                {
                    Destroy(child.gameObject);
                }
            }

        }

        public void ResetPage()
        {
            _searchInputField.text = string.Empty;
            _timelineGraphTransform.gameObject.SetActive(false);
            _timelineListTransform.gameObject.SetActive(false);
        }

        public void ToggleLoading(bool isLoading)
        {
            _progressBar.transform.gameObject.SetActive(isLoading);
        }
    }
}