﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui
{
    public class BreadCrumbsBar : MonoBehaviour
    {
        [SerializeField] private Button _homeButton;

        [SerializeField] private HorizontalLayoutGroup _layoutGroup;
        [SerializeField] private TMP_Text _caseNameText;
        [SerializeField] private TMP_Text _pageNameText;
        [SerializeField] private TMP_Text _termText;

        public void Init()
        {
            _caseNameText.text = string.Empty;
            _pageNameText.text = string.Empty;
            _termText.text = string.Empty;

            GetComponent<RectTransform>().ForceUpdateRectTransforms();
        }

        public void SetCaseNameText(string caseName)
        {
            _caseNameText.text = caseName;

            _layoutGroup.GetComponent<RectTransform>().ForceUpdateRectTransforms();
        }

        public void SetTextAndButtonLinks(string pageName)
        {
            _pageNameText.text = pageName;

            _layoutGroup.GetComponent<RectTransform>().ForceUpdateRectTransforms();
        }

        public void SetSearchTermText(string term)
        {
            _termText.text = term;

            _layoutGroup.GetComponent<RectTransform>().ForceUpdateRectTransforms();
        }
    }
}