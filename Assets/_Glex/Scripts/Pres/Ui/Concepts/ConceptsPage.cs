﻿using System;
using Glex.Core;
using Glex.Core.Managers;
using System.Collections.Generic;
using System.Linq;
using Glex.IO.Serializable;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Concepts
{
    public class ConceptsPage : MonoBehaviour
    {
        [Header("New Concept:")]
        [SerializeField] private Button _newConceptButton;
        [SerializeField] private VerticalLayoutGroup _conceptList;
        [SerializeField] private StoredConceptPrefab _conceptPrefab;

        [Header("Concept Search:")]
        [SerializeField] private TMP_InputField _searchInput;
        [SerializeField] private Button _searchButton;

        [Header("Concept Input:")] 
        [SerializeField] private RectTransform _conceptInputTransform;
        [SerializeField] private GridLayoutGroup _relatedPhrasesGrid;
        [SerializeField] private Button _addAllButton;
        [SerializeField] private Button _addSelectedButton;
        [SerializeField] private TMP_InputField _conceptNameInput;
        [SerializeField] private TMP_InputField _conceptDescriptionInput;
        [SerializeField] private Button _addConceptButton;
        [SerializeField] private TermSuggestionPrefab _termPrefab;

        // private values for Concept
        private Guid _conceptId;
        private string _name => _conceptNameInput.text;
        private string _desc => _conceptDescriptionInput.text;
        private List<string> _searchTerms;
        private List<string> _ignoreTerms;


        public void Init()
        {
            _conceptId = Guid.Empty;
            _searchTerms = new List<string>();
            _ignoreTerms = new List<string>();

            ToggleOffAndResetConceptPage();

            _newConceptButton.onClick.AddListener(ToggleOffAndResetConceptPage);
            _searchInput.onEndEdit.AddListener(CreateNewConcept);
            _addConceptButton.onClick.AddListener(CreateAndSaveConcept);
            _addSelectedButton.onClick.AddListener(AddSelectedTerms);
            _addAllButton.onClick.AddListener(AddAllTerms);

            RefreshConceptsList();
        }

        public void CreateNewConcept(string searchWord)
        {
            if (!searchWord.Equals(string.Empty))
            {
                ClearRelatedTermsList();
                _searchTerms.Add(searchWord);
                MainManager.Instance.StartConceptSearch(searchWord);

                MainManager.Instance.SearchCanvas.BreadCrumbsBar.SetSearchTermText(searchWord);
            }
        }

        private void ToggleOffAndResetConceptPage()
        {
            _conceptInputTransform.gameObject.SetActive(false);
            _searchInput.text = string.Empty;
            _conceptNameInput.text = string.Empty;
            _conceptDescriptionInput.text = string.Empty;
            _conceptId = Guid.Empty;
            _searchTerms = new List<string>();
            _ignoreTerms = new List<string>();

            ClearRelatedTermsList();
        }

        private void ClearRelatedTermsList()
        {
            foreach (Transform child in _relatedPhrasesGrid.transform)
            {
                Destroy(child.gameObject);
            }
        }

        public void ToggleAndSetConceptValues(List<RelatedPhrasePresModel> phrases)
        {
            _conceptInputTransform.gameObject.SetActive(true);

            foreach (var phrase in phrases)
            {
                if (_searchTerms.Contains(phrase.Phrase))
                {
                    var termPrefab = Instantiate(_termPrefab, _relatedPhrasesGrid.transform);
                    termPrefab.Init(phrase);
                    termPrefab.ToggleSelected();
                }
                else
                {
                    var termPrefab = Instantiate(_termPrefab, _relatedPhrasesGrid.transform);
                    termPrefab.Init(phrase);
                }
            }
        }

        public void AddSelectedTerms()
        {
            foreach (Transform child in _relatedPhrasesGrid.transform)
            {
                var prefab = child.GetComponent<TermSuggestionPrefab>();

                if (prefab.Selected)
                {
                    if (!_searchTerms.Contains(prefab.GetTerm()))
                    {
                        _searchTerms.Add(prefab.GetTerm());
                    }
                }
            }
        }

        public void AddAllTerms()
        {
            foreach (Transform child in _relatedPhrasesGrid.transform)
            {
                var prefab = child.GetComponent<TermSuggestionPrefab>();

                if (!_searchTerms.Contains(prefab.GetTerm()))
                {
                    _searchTerms.Add(prefab.GetTerm());
                }
            }
        }

        private void CreateAndSaveConcept()
        {
            if(_conceptId.Equals(Guid.Empty))
            {
                MainManager.Instance.SaveOrReplaceConcept(_name, _desc, _searchInput.text, _searchTerms.ToArray(), _ignoreTerms.ToArray());
            }
            else
            {
                MainManager.Instance.SaveOrReplaceConcept(_conceptId, _name, _desc, _searchInput.text, _searchTerms.ToArray(), _ignoreTerms.ToArray(), new string[0]);
            }

            RefreshConceptsList();
        }

        public void OpenConcept(ConceptPresModel concept)
        {
            ToggleOffAndResetConceptPage();

            _conceptId = concept.ContentId;

            _searchInput.text = concept.InitialSearchWord;
            _searchTerms = concept.SearchTerms.ToList();

            CreateNewConcept(concept.InitialSearchWord);
            
            _conceptNameInput.text = concept.Name;
            _conceptDescriptionInput.text = concept.Description;
        }

        public void RefreshConceptsList()
        {
            foreach (Transform child in _conceptList.transform)
            {
                Destroy(child.gameObject);
            }

            var concepts = MainManager.Instance.GetConcepts();

            foreach (var concept in concepts)
            {
                var conceptPrefab = Instantiate(_conceptPrefab, _conceptList.transform);
                conceptPrefab.Init(concept);
            }
        }
    }
}