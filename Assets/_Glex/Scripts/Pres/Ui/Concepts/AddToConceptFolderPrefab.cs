﻿using Glex.Core;
using Glex.Core.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Concepts
{
    public class AddToConceptFolderPrefab : ConceptBasePrefab
    {
        [SerializeField] private Image _icon;
        [SerializeField] private Button _addConceptFilesButton;

        public void Init(ConceptPresModel concept)
        {
            Concept = concept;
            ConceptNameText.text = concept.Name;

            _addConceptFilesButton.onClick.AddListener(AddFile);
        }

        private void AddFile()
        {
            MainManager.Instance.SearchCanvas.AddToFolderDialog.AddDocumentToFolder(Concept);
        }
    }
}