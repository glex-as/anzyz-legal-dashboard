﻿using Glex.Core.Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Concepts
{
    public class PageButtonPrefab : MonoBehaviour
    {
        [SerializeField] private Image _bgImage;
        public Toggle SetPageButton;
        [SerializeField] private TMP_Text _text;
        [SerializeField] private Color _selectedColor;

        private int _pageNumber;

        public bool IsActive;

        public void Init(int pageNumber)
        {
            _pageNumber = pageNumber;
            _text.text = pageNumber.ToString();
            SetPageButton.onValueChanged.AddListener(SetPageActive);
        }

        public void SetPageActive(bool isOn)
        {
            IsActive = isOn;
            //_bgImage.color = _selectedColor;

            MainManager.Instance.SearchCanvas.Terms.FillDocumentResultList(MainManager.Instance.GetPaginatedResults(_pageNumber));
        }
    }
}