﻿using Glex.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Concepts
{
    public class TermSuggestionPrefab : MonoBehaviour
    {
        [SerializeField] private Image _bgImage;
        [SerializeField] private Button _toggleButton;
        [SerializeField] private TMP_Text _text;

        [SerializeField] private Color _unselectedColor;
        [SerializeField] private Color _selectedColor;

        private RelatedPhrasePresModel _phrase;

        public bool Selected;
        public string Phrase;

        public void Init(RelatedPhrasePresModel phraseModel)
        {
            _phrase = phraseModel;
            Phrase = phraseModel.Phrase;
            _text.text = _phrase.Phrase;
            _toggleButton.onClick.AddListener(ToggleSelected);
            Selected = false;
        }

        public void ToggleSelected()
        {
            Selected = !Selected;

            if (Selected)
            {
                _bgImage.color = _selectedColor;
            }
            else
            {
                _bgImage.color = _unselectedColor;
            }
        }

        public string GetTerm()
        {
            return _phrase.Phrase;
        }
    }
}