﻿using Glex.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Concepts
{
    public class ConceptBasePrefab : MonoBehaviour
    {
        [SerializeField] protected Image BgImage;
        [SerializeField] protected TMP_Text ConceptNameText;

        protected ConceptPresModel Concept;
    }
}