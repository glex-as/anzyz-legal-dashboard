﻿using Glex.Core;
using Glex.Core.Managers;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Glex.Pres.Ui.Concepts
{
    public class AddConceptToSearchInputPrefab : MonoBehaviour
    {
        [SerializeField] private TMP_Dropdown _conceptDropdown;

        private List<ConceptPresModel> _concepts;

        public void Init()
        {
            _conceptDropdown.onValueChanged.AddListener(AddConcept);

            RefreshOptions();
        }

        public void RefreshOptions()
        {
            _concepts = MainManager.Instance.GetConcepts();

            _conceptDropdown.ClearOptions();

            var optionsList = new List<string>() { "Add" };

            foreach (var concept in _concepts)
            {
                optionsList.Add(concept.Name);
            }

            _conceptDropdown.AddOptions(optionsList);
        }

        private void AddConcept(int dropdownValue)
        {
            if (dropdownValue > 0)
            {
                var conceptToAdd = _concepts[dropdownValue - 1];

                MainManager.Instance.SearchCanvas.Terms.AddConceptToSearch(conceptToAdd);
            }
        }

        public void ResetDropdown()
        {
            _conceptDropdown.SetValueWithoutNotify(0);
        }
    }
}