﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConceptList : MonoBehaviour
{
    public GameObject newConceptPrefab;

    public void InstansiateButton()
    {
        Instantiate(newConceptPrefab, this.transform);
    }
}
