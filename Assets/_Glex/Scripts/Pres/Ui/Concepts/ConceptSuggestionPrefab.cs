﻿using Glex.Core;
using Glex.Core.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Concepts
{
    public class ConceptSuggestionPrefab : ConceptBasePrefab
    {
        [SerializeField] private Button _removeButton;
        
        public void Init(ConceptPresModel concept)
        {
            Concept = concept;
            ConceptNameText.text = concept.Name;
            _removeButton.onClick.AddListener(RemoveConcept);
        }

        private void RemoveConcept()
        {
            MainManager.Instance.SearchCanvas.Terms.RemoveConceptFromSearch(Concept);

            Destroy(this.transform.gameObject);
        }
    }
}