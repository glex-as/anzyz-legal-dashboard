﻿using Glex.Core;
using Glex.Core.Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Concepts
{
    public class StoredConceptPrefab : ConceptBasePrefab
    {
        [SerializeField] private Button _openConceptButton;
        [SerializeField] private Image _iconImage;
        [SerializeField] private TMP_Text _descriptionText;
        [SerializeField] private Toggle _meatballButton;
        [SerializeField] private Button _deleteButton;

        public void Init(ConceptPresModel conceptModel)
        {
            Concept = conceptModel;
            ConceptNameText.text = Concept.Name;
            
            _descriptionText.text = Concept.Description;
            _openConceptButton.onClick.AddListener(OpenConcept);
            _deleteButton.onClick.AddListener(DeleteConcept);
        }

        private void DeleteConcept()
        {
            MainManager.Instance.DeleteConcept(Concept.ContentId);
        }

        private void OpenConcept()
        {
            MainManager.Instance.SearchCanvas.Concepts.OpenConcept(Concept);
        }
    }
}
