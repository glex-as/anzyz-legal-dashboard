﻿using Glex.Core;
using Glex.Core.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Concepts
{
    public class ConceptFolderPrefab : ConceptBasePrefab
    {
        [SerializeField] private Image _icon;
        [SerializeField] private Button _loadConceptFilesButton;

        public void Init(ConceptPresModel concept)
        {
            Concept = concept;
            ConceptNameText.text = concept.Name;

            _loadConceptFilesButton.onClick.AddListener(LoadFiles);
        }

        private void LoadFiles()
        {
            MainManager.Instance.SearchCanvas.Documents.LoadDocumentsFromConcept(Concept);
        }
    }
}