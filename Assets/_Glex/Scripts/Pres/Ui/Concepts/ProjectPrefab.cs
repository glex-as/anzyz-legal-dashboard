﻿using Glex.Core.Managers;
using Glex.IO.Serializable;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Concepts
{
    public class ProjectPrefab : MonoBehaviour
    {
        [SerializeField] private Image _bgImage;
        //[SerializeField] private Image _userProfileImage;
        [SerializeField] private Button _openProjectButton;
        [SerializeField] private TMP_Text _projectNameText;
        [SerializeField] private TMP_Text _projectDescriptionText;

        private SerializableProject _project;

        public void Init(SerializableProject project)
        {
            _project = project;

            _projectNameText.text = project.Name;
            _projectDescriptionText.text = project.Description;

            _openProjectButton.onClick.AddListener(OpenProject);
        }

        private void OpenProject()
        {
            MainManager.Instance.OpenProject(_project);
        }
    }
}