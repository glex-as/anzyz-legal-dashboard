﻿using System.IO;
using Glex.Core;
using Glex.Core.Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Documents
{
    public class DocumentPreviewFullscreenPrefab : DocumentTextHighlightBase
    {
        [Header("Top bar:")]
        [SerializeField] private TMP_Text _fileNameText;
        [SerializeField] private Button _downloadButton;
        [SerializeField] private Button _addToFolderButton;
        [SerializeField] private Button _bookmarkButton;

        [SerializeField] private TMP_Text _messageIdText;
        [SerializeField] private TMP_Text _subjectText;
        [SerializeField] private TMP_Text _dateText;

        [SerializeField] private TMP_Text _fromEmailText;
        [SerializeField] private TMP_Text _fromNameText;

        [SerializeField] private TMP_Text _toEmailText;
        [SerializeField] private TMP_Text _toNameText;
        
        [SerializeField] private TMP_Text _ccEmailText;

        [SerializeField] private TMP_Text _mailNameText;

        public void Init()
        {
            _messageIdText.text = string.Empty;
            _subjectText.text = string.Empty;
            _dateText.text = string.Empty;
            _fromEmailText.text = string.Empty;
            _fromNameText.text = string.Empty;
            _toEmailText.text = string.Empty;
            _toNameText.text = string.Empty;
            _ccEmailText.text = string.Empty;
            _fileNameText.text = string.Empty;
            _mailNameText.text = string.Empty;

            _addToFolderButton.onClick.AddListener(AddToFolder);
            
            _bookmarkButton.onClick.AddListener(Bookmark);
        }

        public void SetContent(DocumentPresModel doc, EmailPresModel email, string searchWord)
        {
            _downloadButton.onClick.RemoveAllListeners();
            _downloadButton.onClick.AddListener(() => Download(doc.FilePath));

            HighlightText(doc, email, searchWord);

            _messageIdText.text = email.MessageId;
            _subjectText.text = email.Subject;
            _dateText.text = email.Date;
            _fromEmailText.text = $"<{email.FromEmail}>";
            _fromNameText.text = email.XFrom;
            _toEmailText.text = $"<{email.ToEmails}>";
            _toNameText.text = email.XTo;
            _ccEmailText.text = email.XBcc;
            _fileNameText.text = email.XFileName;
            _mailNameText.text = email.XFileName;
        }

        private void AddToFolder()
        {
            MainManager.Instance.OpenSaveToFolderDialog(Document);
        }

        private void Download(string url)
        {
            // Open document based on url / file path
            //Application.OpenURL(url);

            OpenFile(url);
        }

        private void Bookmark()
        {

        }

        private void OpenFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return;
            }

            // combine the arguments together
            // it doesn't matter if there is a space after ','
            string argument = "/select, \"" + filePath + "\"";

            System.Diagnostics.Process.Start("explorer.exe", argument);
        }

    }
}
