﻿using System;
using System.Collections.Generic;
using System.Linq;
using Glex.Core;
using TMPro;
using UnityEngine;

namespace Glex.Pres.Ui.Documents
{
    public class DocumentTextHighlightBase : MonoBehaviour
    {
        [SerializeField] protected TMP_Text MainText;

        protected DocumentPresModel Document;

        public Color NormalColor;
        public Color HighlightColor;
        public Color SearchHighlightColor;

        private string NormalColorString => ColorUtility.ToHtmlStringRGB(NormalColor);
        private string HighlightColorString => ColorUtility.ToHtmlStringRGB(HighlightColor);
        private string SearchHighlightColorString => ColorUtility.ToHtmlStringRGB(SearchHighlightColor);

        public void HighlightText(DocumentPresModel doc, EmailPresModel email, string searchWord)
        {
            Document = doc;

            var highlightedWordsIndexes = FindHighlightedWords(doc.RawText, doc.CategoryPhrases);
            string highlightedText = SetHighlightedWords(doc.RawText, doc.CategoryPhrases, highlightedWordsIndexes, HighlightColorString);

            if (!searchWord.Equals(string.Empty))
            {
                var searchInputTerms = new[] { searchWord };
                var searchHighlightedWordsIndexes = FindHighlightedWords(highlightedText, searchInputTerms);
                highlightedText = SetHighlightedWords(highlightedText, searchInputTerms, searchHighlightedWordsIndexes, SearchHighlightColorString);
            }

            highlightedText = AddLineBreaks(highlightedText);

            MainText.text = highlightedText;
        }

        private string AddLineBreaks(string text)
        {
            string modifiedText = text.Replace("-----Original Message-----", "\n\n-----Original Message-----\n");

            modifiedText = modifiedText.Replace("---------------------- Forwarded by", "\n\n---------------------- Forwarded by");
            
            modifiedText = modifiedText.Replace("---------------------------------------------------------------- The information transmitted is intended",
                "\n\n---------------------------------------------------------------- The information transmitted is intended");

            return modifiedText;
        }

        private List<int> FindHighlightedWords(string text, string[] phrases)
        {
            List<int> indexes = new List<int>();

            foreach (var phrase in phrases)
            {
                indexes.AddRange(FindWord(text.ToLower(), phrase.ToLower()));
            }

            return indexes;
        }

        private List<int> FindWord(string textString, string word)
        {
            List<int> indexes = new List<int>();

            int index = 0;
            while (index != -1)
            {
                index = textString.IndexOf(word, index, StringComparison.Ordinal);
                if (index != -1)
                {
                    indexes.Add(index);
                    index++;
                }
            }
            return indexes;
        }

        private string SetHighlightedWords(string text, string[] phrases, List<int> indexes, string highlightColor)
        {
            if (indexes.Count <= 0) return text;

            indexes.Sort();

            List<string> subStrings = new List<string>();

            string outputString = string.Empty;

            string startString = text.Substring(0, indexes[0]);
            subStrings.Add(startString);

            foreach (int i in indexes.Distinct())
            {
                if (i != -1 && i < text.Length)
                {
                    string subString = text.Substring(i);

                    subStrings.Add(subString);
                }
            }

            foreach (var s in subStrings)
            {
                foreach (var phrase in phrases)
                {
                    if (s.StartsWith(phrase))
                    {
                        string newPhrase = $"<color=#{highlightColor}>{phrase}<color=#{NormalColorString}>";

                        string modifiedLine = s.Replace(phrase, newPhrase);

                        outputString += modifiedLine;
                    }
                }
            }

            return outputString;
        }

    }
}