﻿using Glex.Core;
using Glex.Core.Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Documents
{
    public class DocumentSingleLinePrefab : DocumentTextHighlightBase
    {
        [SerializeField] private Button _openDocumentButton;
        [SerializeField] private TMP_Text _nameText;

        public void Init(/*DocumentPresModel doc, */EmailPresModel email)
        {
            //Document = doc;

            _nameText.text = email.XFileName;
            MainText.text = email.MainText;

            _openDocumentButton.onClick.AddListener(OpenDocumentPreview);
        }

        private void OpenDocumentPreview()
        {
            //MainManager.Instance.SearchCanvas.OpenDocumentPreview(Document);
        }
    }
}
