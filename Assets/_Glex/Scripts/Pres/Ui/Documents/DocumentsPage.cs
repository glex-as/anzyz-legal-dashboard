﻿using Glex.Core;
using Glex.Core.Managers;
using Glex.Pres.Ui.Concepts;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Documents
{
    public class DocumentsPage : MonoBehaviour
    {
        [SerializeField] private GridLayoutGroup _conceptsGrid;
        [SerializeField] private ConceptFolderPrefab _conceptPrefab;


        [SerializeField] private VerticalLayoutGroup _documentList;
        [SerializeField] private DocumentSingleLinePrefab _documentSingleLinePrefab;

        public void Init()
        {
            RefreshConcepts();
            ClearDocumentList();
        }

        public void RefreshConcepts()
        {
            ClearConceptList();

            var concepts = MainManager.Instance.GetConcepts();

            foreach (var concept in concepts)
            {
                var prefab = Instantiate(_conceptPrefab, _conceptsGrid.transform);
                prefab.Init(concept);
            }
        }

        private void ClearConceptList()
        {
            if (_conceptsGrid.transform.childCount > 0)
            {
                foreach (Transform child in _conceptsGrid.transform)
                {
                    Destroy(child.gameObject);
                }
            }
        }

        public void LoadDocumentsFromConcept(ConceptPresModel concept)
        {
            ClearDocumentList();

            foreach (var file in concept.FilePaths)
            {
                var prefab = Instantiate(_documentSingleLinePrefab, _documentList.transform);

                DocumentPresModel document = null;
                EmailPresModel email = MainManager.Instance.GetDocumentFromFilePath(file);
                
                prefab.Init(/*document, */email);
            }
        }

        private void ClearDocumentList()
        {
            if (_documentList.transform.childCount > 0)
            {
                foreach (Transform child in _documentList.transform)
                {
                    Destroy(child.gameObject);
                }
            }
        }
    }
}