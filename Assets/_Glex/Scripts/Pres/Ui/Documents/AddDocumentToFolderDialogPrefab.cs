﻿using Glex.Core;
using Glex.Core.Managers;
using Glex.Pres.Ui.Concepts;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Documents
{
    public class AddDocumentToFolderDialogPrefab : MonoBehaviour
    {
        [SerializeField] private TMP_InputField _findFolderInputField;
        [SerializeField] private Button _findFolderButton;
        [SerializeField] private GridLayoutGroup _conceptsGrid;
        [SerializeField] private AddToConceptFolderPrefab _conceptPrefab;
        [SerializeField] private TMP_InputField _createFolderInputField;
        [SerializeField] private Button _createFolderButton;

        private DocumentPresModel _document;

        public void Init()
        {
            _findFolderInputField.text = string.Empty;

            RefreshConcepts();
        }

        public void RefreshConcepts()
        {
            ClearConceptList();

            var concepts = MainManager.Instance.GetConcepts();

            foreach (var concept in concepts)
            {
                var prefab = Instantiate(_conceptPrefab, _conceptsGrid.transform);
                prefab.Init(concept);
            }
        }

        private void ClearConceptList()
        {
            if (_conceptsGrid.transform.childCount > 0)
            {
                foreach (Transform child in _conceptsGrid.transform)
                {
                    Destroy(child.gameObject);
                }
            }
        }

        public void SetContent(DocumentPresModel document)
        {
            _document = document;
        }

        public void AddDocumentToFolder(ConceptPresModel concept)
        {
            MainManager.Instance.AddDocumentToConcept(_document, concept);
        }
    }
}