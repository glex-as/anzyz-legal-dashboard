﻿using Glex.Core.Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui.Documents
{
    public class SettingsDialogPrefab : MonoBehaviour
    {
        [SerializeField] private TMP_InputField _setFolderInputField;
        [SerializeField] private Button _setFolderButton;
        [SerializeField] private TMP_InputField _pageSizeInputField;
        [SerializeField] private TMP_InputField _maxPhrasesInputField;
        [SerializeField] private TMP_InputField _randInputField;

        public void Init(int pageSize, int maxPhrases, int rand)
        {
            _setFolderInputField.text = MainManager.Instance.EnronDataBaseFolder;
            _setFolderButton.onClick.AddListener(() => MainManager.Instance.SetEnronMailBaseFolder(_setFolderInputField.text));

            _pageSizeInputField.text = pageSize.ToString();
            _maxPhrasesInputField.text = maxPhrases.ToString();
            _randInputField.text = rand.ToString();

            _pageSizeInputField.onEndEdit.AddListener(SetPageSize);
            _maxPhrasesInputField.onEndEdit.AddListener(SetMaxPhrases);
            _randInputField.onEndEdit.AddListener(SetRandNumber);

            if (_setFolderInputField.text.Equals(string.Empty))
            {
                this.gameObject.SetActive(true);
            }
            else
            {
                this.gameObject.SetActive(false);
            }
        }

        public void RefreshFolderInputText()
        {
            _setFolderInputField.text = MainManager.Instance.EnronDataBaseFolder;
        }

        public void SetPageSize(string sizeText)
        {
            int size = int.Parse(sizeText);

            MainManager.Instance.SetPageSize(size);
        }

        private void SetMaxPhrases(string maxPhrasesText)
        {
            int maxPhrases = int.Parse(maxPhrasesText);

            MainManager.Instance.SetMaxPhrases(maxPhrases);
        }

        private void SetRandNumber(string randText)
        {
            int rand = int.Parse(randText);

            MainManager.Instance.SetRandSize(rand);
        }

    }
}