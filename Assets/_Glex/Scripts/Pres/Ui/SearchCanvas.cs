﻿using Glex.Core;
using Glex.Core.Managers;
using Glex.Pres.Ui.Concepts;
using Glex.Pres.Ui.Documents;
using Glex.Pres.Ui.Terms;
using UnityEngine;
using UnityEngine.UI;

namespace Glex.Pres.Ui
{
    public class SearchCanvas : MonoBehaviour
    {
        [SerializeField] private Image _bgDark;
        [SerializeField] private Image _bgLight;

        [SerializeField] private RectTransform _contentContainer;
        [SerializeField] private RectTransform _sideBarContainer;
        [SerializeField] private RectTransform _breadCrumbsContainer;

        [Header("Main pages:")]
        public TermsPage Terms;
        public ConceptsPage Concepts;
        public TimelinePage Timeline;
        public DocumentsPage Documents;

        public BreadCrumbsBar BreadCrumbsBar;
        public LeftSideMenuBar SideMenuBar;
        public DocumentPreviewFullscreenPrefab DocumentPreviewOverlay;
        public AddDocumentToFolderDialogPrefab AddToFolderDialog;
        public SettingsDialogPrefab SettingsDialog;

        public void Init()
        {
            Terms.Init();
            Concepts.Init();
            Timeline.Init();
            Documents.Init();

            SideMenuBar.Init(this);
            BreadCrumbsBar.Init();
            DocumentPreviewOverlay.Init();
            DocumentPreviewOverlay.gameObject.SetActive(false);
            AddToFolderDialog.Init();
            AddToFolderDialog.gameObject.SetActive(false);
            SettingsDialog.Init(MainManager.Instance.PageSize, MainManager.Instance.MaxPhrases, MainManager.Instance.Rand);

            OpenTermsPage();
        }

        public void OpenDocumentPreview(DocumentPresModel doc)
        {
            var email = MainManager.Instance.GetDocumentFromFilePath(doc.FilePath);

            DocumentPreviewOverlay.SetContent(doc, email, MainManager.Instance.CurrentSearchTerm);
            DocumentPreviewOverlay.gameObject.SetActive(true);
        }

        public void OpenAddToFolderDialog(DocumentPresModel doc)
        {
            AddToFolderDialog.SetContent(doc);
            AddToFolderDialog.RefreshConcepts();
            AddToFolderDialog.gameObject.SetActive(true);
        }

        public void OpenSettingsDialog()
        {
            SettingsDialog.gameObject.SetActive(true);
        }

        public void OpenTermsPage()
        {
            Terms.gameObject.SetActive(true);
            Concepts.gameObject.SetActive(false);
            Timeline.gameObject.SetActive(false);
            Documents.gameObject.SetActive(false);

            Terms.AddToSearchInput.RefreshOptions();

            BreadCrumbsBar.SetTextAndButtonLinks("Terms");
        }

        public void OpenConceptsPage()
        {
            Terms.gameObject.SetActive(false);
            Concepts.gameObject.SetActive(true);
            Timeline.gameObject.SetActive(false);
            Documents.gameObject.SetActive(false);

            BreadCrumbsBar.SetTextAndButtonLinks("Concepts");
        }

        public void OpenTimelinePage()
        {
            Terms.gameObject.SetActive(false);
            Concepts.gameObject.SetActive(false);
            Timeline.gameObject.SetActive(true);
            Documents.gameObject.SetActive(false);



            BreadCrumbsBar.SetTextAndButtonLinks("Timeline");
        }

        public void OpenDocumentsPage()
        {
            Terms.gameObject.SetActive(false);
            Concepts.gameObject.SetActive(false);
            Timeline.gameObject.SetActive(false);
            Documents.gameObject.SetActive(true);

            Documents.RefreshConcepts();

            BreadCrumbsBar.SetTextAndButtonLinks("Documents");
        }
    }
}
